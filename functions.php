<?php

    use Cuisine\Wrappers\Script;
	use \Cuisine\Utilities\Url;
    use \Cuisine\View\Nav;

    //register menu locations:
    Nav::register( array( 'Main', 'Top', 'Footer', 'Mobile' ) );

    require_once('lib/helpers.php');
    require_once('lib/enqueues.php');

    // disables all comments ons site
    require_once('lib/disable-comments.php');
    
    // renames blog "posts" to news 
    require_once('lib/posts-to-news.php');

    // creates simple locations json endpoint 
    require_once('lib/api-routing.php');

    //acf options page 
    require_once('lib/acf-options-page.php');

    //custom image sizes 
    require_once('lib/image-sizes.php');

    //custom shortcodes
    require_once('lib/shortcodes.php');
    
