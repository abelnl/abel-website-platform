<?php


    get_header();


    echo '<div class="main-container">';

     //Show breadcrumbs 
    if ( !is_home() && !is_page('home') && !is_front_page() ) 
        get_template_part( 'partials/breadcrumbs' );

    //Show fixed toptasks 
    get_template_part( 'partials/section-toptasks-fixed' );

    the_sections();  
    
    get_footer();  

   
?>