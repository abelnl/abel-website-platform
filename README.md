Abel Parent Theme
===========================

The default abel parent theme. Only use this theme in conjunction with the [abel child theme](https://bitbucket.org/abelnl/abel-child-theme/)

---

## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| PHP >= 5.4.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php) |
| PHP Shorttags	  | `php.ini` 	 | [enable shorttags](https://stackoverflow.com/questions/2185320/how-to-enable-php-short-tags) |
| Abel child theme | - | [install the child theme](https://bitbucket.org/abelnl/abel-child-theme) |

---

## Installation

Installation of this theme out-of-the-box is pretty easy: 

1. Clone it: `git clone git@bitbucket.org:abelnl/abel-website-platform.git`
2. Place that folder in wp-content/themes
3. Install the [abel child theme](https://bitbucket.org/abelnl/abel-child-theme/)
4. Enable the client theme in the WordPress admin.

If you're using the [Abel deployment tools](https://bitbucket.org/abelnl/abel-deployment), you won't have to install anything manually.


---

## Contributing

Everyone is welcome to help [contribute](CONTRIBUTING.md) and improve this project. There are several ways you can contribute:

* Reporting issues
* Suggesting new features
* Writing or refactoring code
* Fixing [issues](https://github.com/cuisine-wp/cuisine/issues)



