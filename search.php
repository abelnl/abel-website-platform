<?php
    get_header();

?>
    <div class="main-container">

        <section class="ac_content a_bg_white a_padding-top-80 a_padding-bottom-40"> 
            <div class="ac_content_container">
                <div class="ac_content_2-3">

                    <h2>Zoeken</h2>

                    <br>

					<?php get_search_form(); ?>

                </div>
            </div>
        </section>


		<section class="ac_content a_bg_brand_lightest-gray a_padding-top-60 a_padding-bottom-60"> 
            <div class="ac_content_container a_justify_flex-start">
                <div class="ac_content_2-3">                    
                    
					<ul class="ac_list-search">
<?php
					if (have_posts()) : while (have_posts()) : the_post(); 

						get_template_part( 'partials/loop', 'archive' );
					 
					endwhile; 
?>	

					</ul>
						
<?php                  
                    else : 
?>
					<br />
					<p>Geen resultaten</p>
							
<?php                  
                    endif; 
?>


                </div>
            </div>
        </section>

<?php
    get_footer(); 

?>


