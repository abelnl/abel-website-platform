<?php  
  
	use Abel\Front\Settings;

   get_template_part( 'partials/header', 'meta' );


   /*

        IT IS ESSENTIAL THAT THE MENU AND THE MAIN CONTAINER COME RIGHT AFTER EACH OTHER IN THE DOM
        Otherwise the CSS cannot be executed properly.
        
    */

  get_template_part( 'partials/menus/menu', Settings::get( 'menutype', '1' ) );    
