
// init svg4everybody
svg4everybody();


// init Scrollmagic controller
var controller = new ScrollMagic.Controller();

$(function(){
    
    // init foundation
    $(document).foundation();

    // fastclick 
    // FastClick.attach(document.body);

    // default slick 
    $('[data-init-slick]').slick();


    /* ANIME
    --------------------------------------------------------------------------------------------*/

    // default anime
    $('[data-anime-type="default"]').each(function(){

        var uuid = guid();
        $(this).attr('data-anime-id', uuid);

        new ScrollMagic.Scene({offset: 200, triggerElement: '[data-anime-id="'+ uuid  +'"]', triggerHook: 'onEnter',  reverse: false })
        .addTo(controller)
        .on("enter", function(e){
            anime({
                targets: ['[data-anime-id="'+ uuid +'"] [data-anime-elem]'],
                opacity: [0, 1],
                duration: 2000,
                delay: function(el, index) {
                    return index * 66;
                }
            });
        })

    });


    // transform-in
    $('[data-anime-type="transform-in"]').each(function(){

        var uuid = guid();
        $(this).attr('data-anime-id', uuid);

        new ScrollMagic.Scene({offset: 200, triggerElement: '[data-anime-id="'+ uuid  +'"]', triggerHook: 'onEnter', reverse: false })
        .addTo(controller)
        .on("enter", function(e){
            anime({
                targets: ['[data-anime-id="'+ uuid +'"] [data-anime-elem]'],
                translateY: ['20px', '0px'],
                opacity: [1],
                duration: 1200,
                elasticity: 0,
                delay: function(el, index) {
                    return index * 66;
                },
                complete: function() {
                    $('[data-anime-id="' + uuid + '"] [data-anime-elem]').css('transform', '');
                }
            });
        })

    });


    // show-scale
    $('[data-anime-type="scale"]').each(function(){

        var uuid = guid();
        $(this).attr('data-anime-id', uuid);

        new ScrollMagic.Scene({offset: 200, triggerElement: '[data-anime-id="'+ uuid  +'"]', triggerHook: 'onEnter',  reverse: false })
        .addTo(controller)
        .on("enter", function(e){
            anime({
                targets: ['[data-anime-id="'+ uuid +'"] [data-anime-elem]'],
                opacity: [.6, 1],
                scale: [1.03, 1.01],
                duration: 2000,
                elasticity: 100,
                delay: function(el, index) {
                    return index * 66;
                }
            });
        })

    });

    $('[data-anime-type="scale-slow"]').each(function(){

        var uuid = guid();
        $(this).attr('data-anime-id', uuid);

        new ScrollMagic.Scene({offset: 200, triggerElement: '[data-anime-id="'+ uuid  +'"]', triggerHook: 'onEnter',  reverse: false })
        .addTo(controller)
        .on("enter", function(e){
            anime({
                targets: ['[data-anime-id="'+ uuid +'"] [data-anime-elem]'],
                opacity: [.3, 1],
                scale: [1.1, 1.02],
                duration: 4000,
                elasticity: 0,
                easing: 'easeOutExpo',
                delay: function(el, index) {
                    return index * 80;
                }
            });
        })

    });


    // anime count
    $('[data-anime-type="count"]').each(function(){

        var uuid = guid();
        $(this).attr('data-anime-id', uuid);

        new ScrollMagic.Scene({offset: 0, triggerElement: '[data-anime-id="'+ uuid  +'"]', triggerHook: 'onEnter', reverse: false })
        .addTo(controller)
        .on("enter", function(e){
            var amount = {
                number: 0
            }
            var total = parseInt($('[data-anime-id="'+ uuid +'"]').data('anime-count'));
            
            anime({
                targets: amount,
                number: total,
                duration: 2000,
                round: true,
                easing: 'easeOutExpo',
                update: function() {
                    $('[data-anime-id="'+ uuid +'"] [data-anime-elem]').text(amount['number'])
                }
            });
        })

    });

    // anime none fix
    $('[data-anime-type="none"] [data-anime-elem]').css('opacity', '1');


    /* GO TO BY SCROLL
    --------------------------------------------------------------------------------------------*/
    $('[data-go-to-by-scroll]').click(function(e){
        e.preventDefault();

        var destination = $(this).attr('data-destination');
        var offset = $(this).attr('data-offset');
        var speed = $(this).attr('data-speed');
        var container = $(this).attr('data-container');

        goToByScroll(destination,offset,speed, container);
    });


    /* POP COOKIE MESSAGE
    --------------------------------------------------------------------------------------------*/
    var cookieMessageShow = Cookies.get("cookie-message");
    
    if(cookieMessageShow == null) {
        Cookies.set("cookie-message", true, { expires : 365 });
        $('body').addClass('s_is-open_cookie-message');
    } else {
        if(cookieMessageShow != 'false') {
            $('body').addClass('s_is-open_cookie-message');
        }
    }
    
    $('[data-toggle-cookie-message]').on('click tap', function() {
        Cookies.set("cookie-message", false);
        $('body').toggleClass('s_is-open_cookie-message');
    });


    /* POP MOBILE MENU
    --------------------------------------------------------------------------------------------*/
    $('[data-toggle-mobile-menu]').on('click tap', function() {
        $('body').toggleClass('s_is-open_menu-mobile');
    });


    /* POP SEARCH INPUT
    --------------------------------------------------------------------------------------------*/
    $('[data-toggle-menu-search]').on('click tap', function(e) {

        e.preventDefault();
        
        $('body').toggleClass('s_is-open_menu-search');

        if($('body').hasClass('s_is-open_menu-search')) {
            $('[data-menu-search]').focus();
        } else {
            $('[data-menu-search]').val('');
        }
    });

    /* SCROLL TO CONTENT
    --------------------------------------------------------------------------------------------*/
    $('.ac_hero_link_to-content').on('click tap', function () {

        var scrollHeight = $('[data-s-type="hero"]').height();

        anime({
            targets: [document.querySelector('html'), document.querySelector('body')], // both to please Safari and Chrome
            scrollTop: parseInt(scrollHeight / 2),
            duration: 600,
            elasticity: 0,
            easing: 'easeOutExpo',
            delay: 0
        });

    });


}); // end jquery scope


/* GLOBAL FUNCTIONS
--------------------------------------------------------------------------------------------*/

function guid() {
function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}
return s4() + s4() + s4() + s4();
}


function goToByScroll(destination, minusOffset, speed, container) {
    if(destination) {
        if(minusOffset === undefined) minusOffset = '0';
        if(speed === undefined) speed = '666';
        if(container === undefined) container = 'html, body';
        
        //console.log(destination, minusOffset, speed, container);

        var top = 0;
        top = ($("#" + destination).position().top - minusOffset).toFixed(0);

        $(container).animate({ scrollTop: top }, speed, "easeInOutQuad");
    }
}