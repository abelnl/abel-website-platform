<?php

if( function_exists('acf_add_options_page') ) {
 
	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Instellingen Abel Installatie',
		'menu_title' 	=> 'Instellingen Abel Installatie',
		'menu_slug' 	=> 'settings-abel-installatie',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
 
}
?>