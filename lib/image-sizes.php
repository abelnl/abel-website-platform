<?php

	add_theme_support( 'post-thumbnails' );
    add_image_size( '400w', 400, 9999 );
    add_image_size( '600w', 600, 9999 );
    add_image_size( '1000w', 1000, 9999 );
    add_image_size( '1600w', 1600, 9999 );
?>