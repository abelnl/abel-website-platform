<?php

    //removes admin bar on website when logged in 
    add_filter('show_admin_bar', '__return_false');

    //removes emojis bloat in head
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');

    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );

    //removes certain elements on pages 
    function remove_post_type_support_for_pages() {
        // UNCOMMENT if you want to remove some stuff
        // Replace 'page' with 'post' or a custom post/content type
        # remove_post_type_support( 'page', 'title' );
        remove_post_type_support( 'page', 'editor' );
        # remove_post_type_support( 'page', 'thumbnail' );
        # remove_post_type_support( 'page', 'page-attributes' );
        # remove_post_type_support( 'page', 'excerpt' );
    }
    add_action( 'admin_init', 'remove_post_type_support_for_pages' );

    //allow yoast access to editor 
    function my_custom_wpseo_manage_options_capability() {

        $manage_options_cap = 'edit_others_posts';

        return $manage_options_cap;
    }
    add_filter( 'wpseo_manage_options', 'my_custom_wpseo_manage_options_capability' );


    //allow editor to change menu
    $role_object = get_role( 'editor' );

    // add $cap capability to this role object
    if( !is_null( $role_object ) ){
        $role_object->add_cap( 'edit_theme_options' );
    }

    //remove image link if inline image is used in WYSIWYG editor 
    function wpb_imagelink_setup() {
        $image_set = get_option( 'image_default_link_type' );
        
        if ($image_set !== 'none') {
            update_option('image_default_link_type', 'none');
        }
    }
    add_action('admin_init', 'wpb_imagelink_setup', 10);

    //outputs debug info to javascript console 
    function debug($data){
        //if(ENVIRONMENT !== 'LOCAL') return;
        if(is_array($data) || is_object($data)){
            echo("<script>console.log('PHP'," . json_encode($data). ");</script>");
        }else{
            echo("<script>console.log('PHP','" . $data . "');</script>");
        }
    }

    //removes p tags surrounding string 
    function strip_p($str){
        return preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $str);
    }

    //image interchange function 
    function get_interchange($image, $breakpoints){
        $str = 'data-interchange="';
        foreach($breakpoints as $breakpoint){

            $imageSizes = $breakpoint[2] ? $breakpoint[2]['sizes'] : $image['sizes'];

            $str .= "[{$imageSizes[$breakpoint[0]]}, {$breakpoint[1]}],";
        }
        $str .= '" style="background-image: url('.$imageSizes[$breakpoint[0]].')"';
        return $str; 
    }

    function string_limit($x, $length = 50, $postfix = '..'){
        if(strlen($x)<=$length){
            return  $x;
        }
        else{
            $y=substr($x,0,$length) . $postfix;
            echo $y;
        }
    }

    //get relative url of current page
    function get_relative_url(){
          
        $current_url = home_url(add_query_arg(array(),$wp->request));
        return str_replace(home_url(), '', $current_url);
    }


    function get_post_by_slug($the_slug){
        $args = array(
            'name'        => $the_slug,
            'post_type'   => 'any',
            'post_status' => 'publish',
            'numberposts' => 1
        );
        $my_posts = get_posts($args);
        if( $my_posts ) {
            return $my_posts[0];
        }else{
            return false;
        }
    }
    
    function get_breakpoint($breakpoint) {
        $breakpoints = array(
            'medium-portrait' => '640px',
            'medium' => '800px',
            'large' => '1025px',
            'xlarge' => '1200px',
            'xxlarge' => '1440px',
        );
        return $breakpoints[$breakpoint];
    }

    function get_svg_symbol($icon, $addSlashes = false){
        
        // $svgUrl = 'http://dev.stijlbreuk.nu/abelenco/werving/content/themes/abelenco-werving/public/assets/symbols.svg#' . $icon ;
        $svgUrl = get_template_directory_uri() . '/public/assets/symbols.svg#' . $icon ;
        $svg = '<svg role="img"><use xlink:href=[URL]></use></svg>';

        if($addSlashes){
            $svg = addslashes($svg);
        }

        $svg = str_replace('[URL]', $svgUrl , $svg);

        return $svg;
    }

    function get_random_content($type = 'image', $size = '1000') {

        $content = '';
        
        $categories = array(
            'CV ketels',
            'Douche accessoires',
            'Baden',
            'Zonnepanelen'
        );
        $locations = array(
            'Tilburg',
            'Oosterhout',
            'Waalwijk',
            'Breda',
            'Kaatsheuvel',
            'Goirle',
            'Riel'
        );
        $images = array(
            get_template_directory_uri() . '/public/assets/img/dummy/1_'.$size.'w.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy/2_'.$size.'w.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy/3_'.$size.'w.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy/4_'.$size.'w.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy/5_'.$size.'w.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy/6_'.$size.'w.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy/7_'.$size.'w.jpg'
        );
        $images_gallery = array(
            get_template_directory_uri() . '/public/assets/img/dummy_gallery/1.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy_gallery/2.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy_gallery/3.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy_gallery/4.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy_gallery/5.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy_gallery/6.jpg',
            get_template_directory_uri() . '/public/assets/img/dummy_gallery/7.jpg'
        );
        $products = array(
            'Remeha calenta + isense + installatie',
            'Remeha avanta + isense + installatie',
            'Remeha tzerra 28c + isense + installatie',
            'Awb te advance + klokthermostaat + installatie',
            'Nefit proline + installatie',
            'Nefit trendline + installatie',
            'Intergas kombi kompakt hre a + installatie',
            'Intergas kombi kompakt hreco + installatie'
        );
        $services = array(
            'Loodgieten',
            'Zonnepanelen',
            'Loodgieten',
            'Service & onderhoud',
            'Gas - water - riolering',
            'Dakbedekking',
            'Sanitair'
        );
        $services_icon = array(
            'icon_service_24u_klok',
            'icon_service_24u',
            'icon_service_airconditioning',
            'icon_service_cv-ketel-1',
            'icon_service_cv-ketel-2',
            'icon_service_dakwerkzaamheden',
            'icon_service_duurzaamheid',
            'icon_service_electriciteit',
            'icon_service_gas',
            'icon_service_gebroken-pijp',
            'icon_service_hand',
            'icon_service_lekkende-kraan',
            'icon_service_luchtbehandeling',
            'icon_service_onderhoud-1',
            'icon_service_onderhoud-2',
            'icon_service_onderhoud-3',
            'icon_service_ontstopper',
            'icon_service_ontzorgexpert',
            'icon_service_riolering',
            'icon_service_sanitair-douche',
            'icon_service_sifon',
            'icon_service_thermometer',
            'icon_service_thermostaat',
            'icon_service_verwarming-1',
            'icon_service_verwarming-2',
            'icon_service_warmtepomp',
            'icon_service_water',
            'icon_service_zonneenergie',
        );
        $titles = array(
            'Vestibulum id ligula porta felis euismod semper.',
            'Nullam id dolor id nibh ultricies vehicula ut id elit.',
            'Sed posuere consectetur est at lobortis.',
            'Etiam porta sem malesuada magna mollis euismod.',
            'Nullam id dolor id nibh ultricies vehicula ut id elit.',
            'Integer posuere erat a ante venenatis dapibus posuere velit aliquet.',
            'Maecenas faucibus mollis interdum.',
            'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'
        );
        $descriptions = array(
            'Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo.',
            'Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.',
            'Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
            'Nullam quis risus eget urna mollis ornare vel eu leo. Donec ullamcorper nulla non metus auctor fringilla.',
            'Maecenas sed diam eget risus varius blandit sit amet non magna. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.',
            'Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Curabitur blandit tempus porttitor.'
        );
        $videos = array(
            'https://stijlbreuk.nl/assets/img/page-over-ons/over-ons.mp4',
            'https://stijlbreuk.nl/assets/img/page-diensten/diensten.mp4',
            'https://stijlbreuk.nl/assets/img/page-werk/werk_background.mp4'
        );

        if($type == 'video'){
            $rand = array_rand($videos);
            $content = $videos[$rand];
        } elseif ($type == 'image') {
            $rand = array_rand($images);
            $content = $images[$rand];
        } elseif ($type == 'image-gallery') {
            $rand = array_rand($images_gallery);
            $content = $images_gallery[$rand];
        } elseif ($type == 'title') {
            $rand = array_rand($titles);
            $content = $titles[$rand];
        } elseif ($type == 'product') {
            $rand = array_rand($products);
            $content = $products[$rand];
        } elseif ($type == 'service') {
            $rand = array_rand($services);
            $content = $services[$rand];
        } elseif ($type == 'service-icon') {
            $rand = array_rand($services_icon);
            $content = $services_icon[$rand];
        } elseif ($type == 'location') {
            $rand = array_rand($locations);
            $content = $locations[$rand];
        } elseif ($type == 'category') {
            $rand = array_rand($categories);
            $content = $categories[$rand];
        } elseif ($type == 'description') {
            $rand = array_rand($descriptions);
            $content = $descriptions[$rand];
        }
        

        return $content;

    }



?>