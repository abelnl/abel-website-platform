<?php

use Cuisine\Utilities\Url;

//before template redirect, register scripts:
add_action( 'template_redirect', function(){

	$path = Url::path('theme');
	$url = Url::parentTheme('public/assets/js/' );

	$deps = ['jquery'];
	
	if( file_exists( $path . '/public/assets/js/app.js') ) 
		$url = Url::theme('public/assets/js/' );
	else
		$url = Url::parentTheme('public/assets/js/' );

	wp_register_script( 'app', $url.'app.js', $deps, '1.1', true );
	
	//wp_register_script( 'section-slick', Url::parentTheme( '/scripts/section-slick.js' ), ['jquery', 'slick' ]);
});


//enqueue the app javascript
add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script('app');

    /*global $wp_the_query;
    if( isset( $wp_the_query->post->ID ) ){
        $meta = get_post_meta( $wp_the_query->post->ID );
        foreach( $meta as $key => $item ){
            if( substr( $key, 0, 12 ) == '_column_type' && $item[0] == 'abel360' ){
                $url = Url::plugin('abel/Assets/js/aframe-v1.0.3.min.js');
                wp_enqueue_script('360column', $url );
                break;
            }
        }
    }*/
});
