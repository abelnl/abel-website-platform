<?php

function change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Nieuws';
    $submenu['edit.php'][5][0] = 'Nieuws';
    $submenu['edit.php'][10][0] = 'Nieuws toevoegen';
    $submenu['edit.php'][16][0] = 'Nieuws Tags';
}
function change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Nieuws';
    $labels->singular_name = 'Nieuws';
    $labels->add_new = 'Nieuws toevoegen';
    $labels->add_new_item = 'Nieuws toevoegen';
    $labels->edit_item = 'Nieuws aanpassen';
    $labels->new_item = 'Nieuws';
    $labels->view_item = 'Bekijk nieuws';
    $labels->search_items = 'Zoek nieuws';
    $labels->not_found = 'Geen nieuws gevonden';
    $labels->not_found_in_trash = 'Geen nieuws in prullenbak';
    $labels->all_items = 'Al het nieuws';
    $labels->menu_name = 'Nieuws';
    $labels->name_admin_bar = 'Nieuws';
}


 
add_action( 'admin_menu', 'change_post_label' );
add_action( 'init', 'change_post_object' );

?>