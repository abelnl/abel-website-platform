<?php

    function button_shortcode( $atts ) {
        
        // example shortcode
        // [button link="acceptatie.abelinstallatie.nl/bierings/contact" text="Meer over ons" classes="v_brand_primary" icon="arrow-right"]
        
        extract( shortcode_atts( array(
            'link'      => '',
            'text'      => '',
            'classes'   => '',
            'icon'      => ''
        ), $atts, 'button' ) );
        
        $iconHtml = ''; 
        $iconClass = '';
        if($icon != '') {
            $iconHtml = '<span class="svg-container"><svg role="img"><use xlink:href="' . get_template_directory_uri() . '/public/assets/symbols.svg#icon_ui_' . $icon .'"/></svg></span>';
            $iconClass = 'v_has-icon-right';
        }
        
        $linkHtml = '';
        if($link != '') $linkHtml = 'href="'.$link.'"'; 
        
        return  '<a '. $linkHtml.' class="button '.$iconClass.'  '.$classes.'"is>'.$text.$iconHtml.'</a>';
    }
    add_shortcode( 'button', 'button_shortcode' );

?>