<?php

//rewrite to load api/locations
function returnApi($originalTemplate) {
    
    if( preg_match('/api\/locations/', $_SERVER['REQUEST_URI']) ){
        return get_template_directory() . '/api/locations.php';
        exit();
    }else{
        return $originalTemplate;
    }
 
}
 
add_action( 'template_include', 'returnApi' );

?>