<?php

    use \Abel\Helpers\TeamMember;
    use \Abel\Helpers\Image;
    
    /*

        TEAM

    */
?>



<section data-s-type="<?= $view->type();?>" data-s-id="<?= $view->s_id();?>" data-s-amount="<?= $view->amount();?>" class="<?= $view->class('ac_content');?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>
        

    <?= $view->title();?>
 
    <div class="ac_team_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>">
 
        <?php if( $query->have_posts() ) while( $query->have_posts() ): $query->the_post();?>
        
        <div class="ac_team_item" data-s-amount-item <?= $view->elementAnimation(); ?>>
            <div class="ac_team_item_container" data-border-bottom>
                <?php 
                   $imgId = TeamMember::avatarId(); 
                   if(!is_null($imgId) ):
                ?>
                <div class="ac_team_item_profile-image_container">
                    <figure class="ac_team_item_profile-image <?= Image::alignment( $imgId ); ?>" data-interchange="[<?= Image::url( $imgId, '600w') ?>, small]" style="background-image: url(<?= Image::url( $imgId, '600w') ?>)"></figure>
                 </div>
                <?php endif; ?>
                <div class="ac_team_item_content">
                    <div class="ac_team_item_name">
                        <?= TeamMember::name(); ?>
                    </div>
                    <div class="ac_team_item_job-description">
                        <?= TeamMember::jobtitle(); ?>
                    </div>
                    <?php if ( TeamMember::telephone() ) : ?>
                    <div class="ac_team_item_telephone">
                        <a href="tel:<?= TeamMember::telephone(); ?>"><?= TeamMember::telephone(); ?></a>
                    </div>
                    <?php 
                    endif; 
                    if ( TeamMember::email() ) :
                    ?>
                    <div class="ac_team_item_email">
                        <a href="mailto:<?= TeamMember::email(); ?>" target="_blank"><?= TeamMember::email(); ?></a>
                    </div>
                    <?php endif;?>

                    <!-- socials -->
                    <?php if( TeamMember::hasSocials() ):?>
                        <div class="ac_team_item_socials">
                        <?php foreach( TeamMember::getSocials() as $social ):?>
                            <a href="<?= $social['link'];?>" class="ac_item_socials_<?= $social['type']?>">
                                <?= get_svg_symbol( 'icon_'.$social['type'] );?>
                            </a>
                        <?php endforeach; ?>
                        </div>
                    <?php endif;?>

                </div>
            </div>
        </div>

        <?php
        endwhile;
        ?>

    </div>
    
    <?php $view->end();?>

</section>