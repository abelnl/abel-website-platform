<?php
    use Cuisine\Wrappers\Script;
    use \Cuisine\Utilities\Url;
    use \Abel\Helpers\Image;
    use \Abel\Helpers\Icon;

    /*

        HERO

    */

    $container = $section;
?>

<section data-s-type="<?= $view->type();?>" data-s-id="<?= $view->s_id();?>" class="<?= $view->class('ac_content');?>" id="<?= $view->id();?>"  <?= $view->sectionAnimation(); ?>>
    <?= $view->title(); ?>
    <div class="ac_hero_container  <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>">
        <div class="ac_hero_slides" data-init-slick data-slick='{
            "mobileFirst": true,
            "dots": <?= $view->heroShowNavigation()?'true':'false'; ?>,
            "slidesToShow": 1,
            "slidesToScroll": 1,
            "prevArrow": <?= $view->heroShowArrows()?'"<button type=\"button\" class=\"slick-prev\">'. get_svg_symbol('icon_ui_arrow-left', true) .'</button>"':'false'; ?>,
            "nextArrow": <?= $view->heroShowArrows()?'"<button type=\"button\" class=\"slick-next\">'. get_svg_symbol('icon_ui_arrow-right', true) .'</button>"':'false'; ?>
        }'>


            <?php
            
            foreach( $container->sections->all() as $section ):
                $content = $section->getColumn();
            ?>

            <div class="ac_hero_slide">
                <div class="ac_hero_slide_inner">
                    <div class="ac_hero_slide_content_container">

                        <div class="ac_hero_slide_content">
                            <div class="ac_hero_slide_content_inner">
                                <div class="ac_hero_slide_content_text">

                                    <div class="ac_hero_slide_content_text_title">
                                        <?php $content->theTitle() ?>
                                    </div>

                                    <div class="ac_hero_slide_content_text_desciption">
                                        <?php $content->theField( 'description' ) ?>
                                    </div>
                                    
                                    <?php if( $content->hasButtons() ):?>
                                        <div class="ac_cta_column">
                                            <?php 
                                            foreach( $content->getField( 'buttons' ) as $button ): 
                                                $button = $button['button'];

                                                if( $button['text'] !== '' ):
                                            ?>
                                            <a href="<?= $button['link'];?>" <?= (array_key_exists('target', $button) && ($button['target'] || $button['target'] == 'true')  && $button['target'] != 'false')?'target="_blank"':'' ?> class="button <?= $button['class'];?> <?= $button['iconClass'];?>">
                                                <?= $button['text'] ?>
                                                <?php if( $button['icon'] != '' && $button['icon'] != 'none' ): ?>
                                                    <span class="svg-container">
                                                        <?= Icon::get( $button['icon'] ); ?>
                                                    </span>
                                                <?php endif; ?>
                                            </a> 
                                            <?php endif;?>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>   

                                </div>
                                
                                <?php if( Image::set( $content->getField( 'image' ) ) ) : ?>
                                <div class="ac_hero_slide_content_image">
                                    <img src="<?= Image::url(null, '1600w');?>" alt="" class="<?= Image::alignment(); ?>">        
                                </div>
                                <?php endif;?>
                            </div>
                        </div>

                    </div>
                </div>
                
                <?php $view->backdrop( $section );?>   
            </div>

            <?php endforeach; //end section loop ?>
        </div>
        
    </div>
    
    <?php if( $view->heroLinkToContent() ):?>
        <button class="ac_hero_link_to-content">
            <?= get_svg_symbol('icon_ui_scroll-down'); ?>
        </button>
    <?php endif;?>

    <?php $view->end();?>
    
</section>