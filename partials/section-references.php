<?php
    use \Cuisine\Wrappers\Script;
    use \Cuisine\Utilities\Url;
    use \Abel\Helpers\Icon;
    use \Abel\Helpers\Reference;

    /*  

        REFERENCES

    */

?>

<section data-s-type="<?= $view->type();?>" data-s-id="<?= $view->s_id();?>" data-s-amount="<?= $view->amount(3);?>" id="<?= $view->id();?>" class="<?= $view->class('ac_content' );?> <?php if( $view->isSlider() ) echo 'v_is-slider';?> <?if ( $view->showNavigation() ) echo 'v_has-dots'; ?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>

  
    <?= $view->title(); ?>

    <div class="ac_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" <?php if( $view->isSlider() ): ?>data-init-slick data-slick='{
            "mobileFirst": true,
            "dots": <?= $view->showNavigation()?>,
            <?= $view->getSliderTime();?>
            "slidesToShow": 1,
            "slidesToScroll": 1,
            "prevArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-prev\">'. get_svg_symbol('icon_ui_arrow-left', true) .'</button>"':'false'; ?>,
            "nextArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-next\">'. get_svg_symbol('icon_ui_arrow-right', true) .'</button>"':'false'; ?>,
            "responsive": [
                {
                    "breakpoint": <?= (int)get_breakpoint('medium-portrait'); ?>,
                    "settings": {
                        "slidesToShow": 2,
                        "slidesToScroll": 2
                    }
                },
                {
                    "breakpoint": <?= (int)get_breakpoint('large'); ?>,
                    "settings": {
                        "slidesToShow": <?= $view->getSlideAmount()?>,
                        "slidesToScroll": <?= $view->getSlideAmount('scroll')?>
                    }
                }
            ]
        }' <?php endif; ?>>

        <?php 
        //load in the grid item for each refence:
        foreach( $query->posts as $reference ):?>
            <?= Reference::gridItem( $reference, $section );?>
        <?php endforeach; ?>

    </div>
    
    <?php $view->end();?>

</section>