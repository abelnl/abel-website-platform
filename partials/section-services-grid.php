<?php

    use Abel\Helpers\Service;

    /*  

        SERVICES GRID

    */
?>


<section data-s-type="<?= $view->type( 'services-grid' );?>" class="<?= $view->class('ac_content');?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>


    <?= $view->title(); ?>


    <div class="ac_grid_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>">

        <div class="ac_grid_outer-column">
            <?php if( isset( $items[1] ) ):?>
            <div class="ac_grid_column">
                <?php foreach( $items[1] as $item ):?>
                    <?= Service::gridItem( $item, $section );?>
                <?php endforeach;?>
            </div>
            <?php endif;?>
            <?php if( isset( $items[2] ) ):?>
            <div class="ac_grid_column">
                <?php foreach( $items[2] as $item ):?>
                    <?= Service::gridItem( $item, $section );?>
                <?php endforeach;?>
            </div>
            <?php endif;?>
        </div>
        <?php if( isset( $items[3] ) ):?>
        <div class="ac_grid_outer-column">
            <?php foreach( $items[3] as $item ):?>
                <?= Service::gridItem( $item, $section );?>
            <?php endforeach;?>
        </div>
        <?php endif;?>
   
    </div>
    <?php $view->end();?>

</section>