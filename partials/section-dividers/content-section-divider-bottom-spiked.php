    <div class="ac_section_divider a_fill_<?= $section_divider_color; ?>" data-s-divider-type="spiked">
        <div class="ac_section_divider_bottom">
            <svg>
                <defs>
                    <pattern id="pattern_spiked" x="0" y="0" width="40" height="21" patternUnits="userSpaceOnUse">
                        <polyline points="40 20 0 20 20 0"/>
                    </pattern>
                </defs>
                <rect x="0" y="0" width="100%" height="21" fill="url(#pattern_spiked)" />
            </svg>
        </div>
    </div>