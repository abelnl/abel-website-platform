    <div class="ac_section_divider a_fill_<?= $section_divider_color; ?>" data-s-divider-type="spiked-small">
        <div class="ac_section_divider_top">
            <svg>
                <defs>
                    <pattern id="pattern_spiked-small" x="0" y="0" width="30" height="17" patternUnits="userSpaceOnUse">
                        <polyline points="30 15 0 15 15 0"/>
                    </pattern>
                </defs>
                <rect x="0" y="0" width="100%" height="17" fill="url(#pattern_spiked-small)" />
            </svg>
        </div>
        <div class="ac_section_divider_bottom">
            <svg>
                <rect x="0" y="0" width="100%" height="17" fill="url(#pattern_spiked-small)" />
            </svg>
        </div>
    </div>