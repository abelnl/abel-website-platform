    <div class="ac_section_divider a_fill_<?= $section_divider_color; ?>" data-s-divider-type="angled-svg">
         <div class="ac_section_divider_bottom">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
                <polygon points="0,100 100,0 100,100"/>
            </svg>
        </div>
    </div>