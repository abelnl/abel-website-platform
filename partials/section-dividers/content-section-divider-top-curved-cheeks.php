    <div class="ac_section_divider a_fill_<?= $section_divider_color; ?>" data-s-divider-type="curved-cheeks">
        <div class="ac_section_divider_top">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                <path d="M451.51,84.84C372.69,62.25,204.81,22.15,0,20V120H1000V23C785.2,23,623,62,547.26,84.45A170.26,170.26,0,0,1,451.51,84.84Z" transform="translate(0 -20)" />
            </svg>
        </div>
    </div>