    <div class="ac_section_divider a_fill_<?= $section_divider_color; ?>" data-s-divider-type="dotted">
        <div class="ac_section_divider_bottom">
            <svg>
                <defs>
                    <pattern id="pattern_dotted" x="0" y="0" width="40" height="20" patternUnits="userSpaceOnUse">
                        <circle cx="20" cy="20" r="20"  />
                    </pattern>
                </defs>
                <rect x="0" y="0" width="100%" height="20" fill="url(#pattern_dotted)" />
            </svg>
        </div>
    </div>