    <div class="ac_section_divider a_fill_<?= $section_divider_color; ?>" data-s-divider-type="smile">
        <div class="ac_section_divider_top">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 200" preserveAspectRatio="none">
                <path d="M500,184.53C250.25,184.53,42.6,104.91,0,0V200H1000V0C957.4,104.91,749.75,184.53,500,184.53Z"/>
            </svg>
        </div>
    </div>