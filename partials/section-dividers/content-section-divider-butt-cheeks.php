    <div class="ac_section_divider" data-s-divider-type="butt-cheeks">
        <div class="ac_section_divider_top">
            <div class="ac_section_divider_cheek_left a_bg_<?= $section_divider_color; ?>"></div>
            <div class="ac_section_divider_cheek_right a_bg_<?= $section_divider_color; ?>"></div>
        </div>
         <div class="ac_section_divider_bottom">
            <div class="ac_section_divider_cheek_left a_bg_<?= $section_divider_color; ?>"></div>
            <div class="ac_section_divider_cheek_right a_bg_<?= $section_divider_color; ?>"></div>
         </div>
    </div>