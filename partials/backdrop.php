<?php $backdrop = $view->getBackdrop(); ?>
<div class="ac_backdrop">
<?php  
    // VIDEO BACKDROP:
    if( $backdrop['type'] == 'video' ):?>

    <div class="ac_backdrop_video-container <?= $backdrop['containerClass'] ?>">
        <video preload="auto" autoplay loop muted playsinline>
            <source src="<?= $backdrop['url'];?>" type="video/mp4">
        </video>
    </div>

    <?php 
    //IMAGE:
    elseif( $backdrop['type'] == 'image' ):?>

        <div class="ac_backdrop_image-container <?= $backdrop['containerClass'] ?>">
            <figure class="ac_backdrop_image <?= $backdrop['imageClass']; ?>" style="background-image: url(<?= $backdrop['url']; ?>);" data-interchange="[<?= $backdrop['600w']; ?>, small], [<?= $backdrop['1000w']; ?>, medium], [<?= $backdrop['1600w']; ?>, large]"></figure>
        </div>

    <?php endif;?>
</div>
