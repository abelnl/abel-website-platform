<?php

    /*

        CTA

    */
    
    use Abel\Helpers\Icon;
    use Abel\Helpers\Social;
    use Abel\Helpers\Image;
    use Abel\Helpers\Button;


?>
<section data-s-type="<?= $view->type(); ?>" data-s-id="<?= $view->s_id();?>" class="<?= $view->class('ac_content');?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>
    <?= $view->title(); ?>
    <div class="ac_cta_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>"> 

        <?php if( !is_null( $column->getTitle() ) ):?>
        <div class="ac_cta_column">
            <?php $column->theTitle();?>
        </div>
        <?php endif;?>

        <?php if( $column->getField( 'description' ) && $column->getField( 'description' ) != '' ): ?>
        <div class="ac_cta_column">
            <?php $column->theField( 'description' )?>
        </div>
        <?php endif; ?>

        <?php if( $column->hasButtons() ):?>
            <div class="ac_cta_column">
                <?php 
                foreach( $column->getField( 'buttons' ) as $button ): 
                    $button = $button['button'];
                ?>
                <a href="<?= $button['link'];?>"  <?= (array_key_exists('target', $button) && ($button['target'] || $button['target'] == 'true') && $button['target'] != 'false')?'target="_blank"':'' ?> class="<?= Button::getClass( $button );?>">
                    <?= $button['text'] ?>
                    <?php if( $button['icon'] != '' && $button['icon'] != 'none' ): ?>
                    <span class="svg-container">
                        <?= Icon::get( $button['icon'] ); ?>
                    </span>
                    <?php endif; ?>
                </a>    
                <?php endforeach ?>
            </div>
        <?php endif; ?>

        <?php if( Image::set( $column->getField( 'image' ) ) ) : ?>
        <div class="ac_cta_column">
            <img src="<?= Image::url(); ?>" alt="" class="<?= Image::alignment(); ?>">
        </div>
        <?php endif; ?>

          <?php 
         $showSocials = $column->getField( 'showsocials' );
         
         if( ($showSocials && $showSocials != 'false') || $showSocials == 'true' ): ?>
        <div class="ac_cta_column">
            <!-- OPTIONAL v_smaller v_smallest-->
            <div class="ac_socials v_smaller">
                <?php foreach( Social::all() as $key => $label ):?>
                    <?php if( Social::hasLink( $key ) ):?>
                    <a href="<?= Social::link( $key );?>" class="ac_socials_link" target="_blank" data-anime-elem>
                        <span class="ac_socials_link_icon">
                            <?= Icon::get('icon_ui_'. $key ); ?>
                        </span>
                        <span class="ac_socials_link_text">
                            <?= $label ?>
                        </span>
                    </a>
                    <?php endif;?>
                <?php endforeach;?>
            </div>
        </div>
        <?php endif; ?>

    </div>

    <?php $view->reset();?>
    <?php $view->divider(); ?>
    <?php $view->backdrop();?>
</section>