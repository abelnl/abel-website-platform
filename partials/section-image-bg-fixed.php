<?php
    use Abel\Helpers\Image;

    $image = $section->getColumn( 'image' );
    $id = $image->getField( 'id' );
?>
<section data-s-type="<?= $view->type();?>" data-s-id="<?= $view->s_id();?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>
    
    <div class="ac_image">
        <figure class="<?= Image::alignment( $id ) ?>" data-interchange="[<?= Image::url( $id, '600w') ?>, small], [<?= Image::url( $id, '1000w') ?>, medium], [<?= Image::url( $id, '1600w') ?>, large]" style="background-image: url(<?= Image::url( $id, '1600w') ?>)"></figure>
    </div>

    <?php $view->end();?>

</section>