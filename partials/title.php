<?php
use Abel\Helpers\Icon;
    
    $button = $view->titleButton();
    $class = '';


        if( $button['class'] != 'none' )
            $class .= ' '.$button['class'];

        if( $button['iconClass'] != 'none' )
            $class .= ' '.$button['iconClass'];


        if( strip_tags( strtolower( $title ) ) !== 'sectie titel' && 
            strip_tags( strtolower( $title ) ) !== 'section title' ):
?>

<div class="ac_heading <?= $view->titleClass();?>">
    <div class="ac_heading_container <?= $view->titleMaxWidth(); ?>" data-anime-elem>
        <div class="ac_heading_title">
            <?= $title ?>
        </div>

        <?php if( $button && $button['link'] != '' ): ?>
        <div class="ac_heading_link">
            <a href="<?= $button['link'];?>" <?= (array_key_exists('target', $button) && ($button['target'] || $button['target'] == 'true') && $button['target'] != 'false')?'target="_blank"':'' ?>  class="button<?= $class?>">
                <?= $button['text'] ?>

                <?php if( isset( $button['icon'] ) && $button['icon'] != 'none' ):?>
                <span class="svg-container">
                    <?= Icon::get( $button['icon'] ); ?>
                </span>
                <?php endif;?>
            </a>         
        </div>  
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>