<?php
    use Cuisine\Wrappers\Script;
    use \Cuisine\Utilities\Url;
    use \Abel\Helpers\Image;
    use \Abel\Helpers\Icon;
    /*

        HERO ALT

    */

    $container = $section;
    global $post;
?>

<section data-s-type="<?= $view->type();?>" data-s-id="<?= $view->s_id();?>" class="<?= $view->class('ac_content');?>" id="<?= $view->id();?>">
    <?= $view->title(); ?>
    <div class="ac_hero_alt_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>">
        <div class="ac_hero_alt_slides a_text_default_light" data-init-slick data-slick='{
            "mobileFirst": true,
            "dots": <?= $view->heroShowNavigation()?'true':'false'; ?>,
            "slidesToShow": 1,
            "slidesToScroll": 1,
            "prevArrow": <?= $view->heroShowArrows()?'"<button type=\"button\" class=\"slick-prev\">'. get_svg_symbol('icon_ui_arrow-left', true) .'</button>"':'false'; ?>,
            "nextArrow": <?= $view->heroShowArrows()?'"<button type=\"button\" class=\"slick-next\">'. get_svg_symbol('icon_ui_arrow-right', true) .'</button>"':'false'; ?>
        }'>


            <?php
            foreach( $container->sections->all() as $section ):

                $content = $section->getColumn();
            ?>

            <div class="ac_hero_alt_slide">
                <div class="ac_hero_alt_slide_inner">
                    <div class="ac_hero_alt_slide_content_container">

                        <div class="ac_hero_alt_slide_content">
                            <div class="ac_hero_alt_slide_content_inner">
                                <div class="ac_hero_alt_slide_content_text">
                                    <?php 
                                    if($content->getTitle()) {
                                        $content->theTitle();
                                    } else {
                                        echo '<h1>'.$post->post_title.'</h1>';
                                    }
                                    ?>         
                                    <?php $content->theField( 'description' ) ?>                     
                                </div>
                                <?php if( Image::set( $content->getField( 'image' ) ) ) : ?>
                                <div class="ac_hero_alt_slide_content_image">
                                    <img src="<?= Image::url(null, '1600w');?>" alt="" class="<?= Image::alignment(); ?>">        
                                </div>
                                <?php endif;?>
                            </div>
                        </div>

                    </div>
                </div>
                
                <?php $view->backdrop( $section );?>

            </div>

            <?php endforeach;?>

        </div>
        
        <?php $view->end();?>

    </div>
    
</section>

