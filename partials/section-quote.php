<?php
    use \Cuisine\Wrappers\Script;
    use \Cuisine\Utilities\Url;
    use Abel\Helpers\Icon;
    use Abel\Helpers\Image;

    /*  

        ITEMS

    */
   
?>

<section data-s-type="<?= $view->type();?>" data-s-id="<?= $view->s_id();?>" data-s-amount="1" class="<?= $view->class('ac_content');?>" <?= $view->sectionAnimation(); ?>>

    <?= $view->title(); ?>

    <div class="ac_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" <?= $view->elementAnimation(); ?>>

        <?php 
            $quote = $column;
            $image = $quote->getField( 'avatar' );
            if( $image['img-id'] == '' )
                $image = null;       
        ?>

        <div class="ac_item" data-s-amount-item >
            <div class="ac_item_container" data-border-bottom>

                <div class="ac_item_content">
                    
                    <div class="ac_item_content_copy">
                        <div class="ac_item_content_copy-above">

                        </div>
                        <div class="ac_item_content_title">
                            <div class="ac_item_content_title_quote-left">
                                <?= Icon::get('icon_ui_quote-left'); ?>
                            </div>
                            <?= $quote->getField('quote'); ?>
                            <div class="ac_item_content_title_quote-right">
                                <?= Icon::get('icon_ui_quote-right'); ?>
                            </div>
                        </div>
                         <div class="ac_item_content_copy-below">

                            <div class="ac_item_author">
                             <?php 
                             if( !is_null( $image ) ):
                                $imgId = $image['img-id'];
                             ?>
                                <div class="ac_item_avatar_container">
                                    <figure class="ac_item_avatar_image <?= Image::alignment( $imgId ); ?>" data-interchange="[<?= Image::url( $imgId, '600w') ?>, small], [<?= Image::url( $imgId, '1000w') ?>, medium], [<?= Image::url( $imgId, '1600w') ?>, large]" style="background-image: url(<?= Image::url( $imgId, '1600w') ?>)"></figure>
                                </div>
                            <?php endif; ?>
                                
                                <div class="ac_item_author_name">
                                    <?= $quote->getField('author'); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php $view->end();?>
</section>