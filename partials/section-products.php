<?php
    use Cuisine\Wrappers\Script;
    use \Cuisine\Utilities\Url;
    use \Abel\Helpers\Product;
    use \Abel\Helpers\Image;

    /*  

        PRODUCTS

    */

?>

<section data-s-type="<?= $view->type('products');?>" data-s-id="<?= $view->s_id();?>" data-s-amount="<?= $view->amount();?>" class="<?= $view->class('ac_content');?> <?php if( $view->isSlider() ) echo 'v_is-slider';?> <?if ( $view->showNavigation() ) echo 'v_has-dots'; ?>" id="<?= $view->id();?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>

    <?= $view->title(); ?>


    <div class="ac_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" <?php if( $view->isSlider() ): ?>data-init-slick data-slick='{
            "mobileFirst": true,
            "dots": <?= $view->showNavigation()?>,
            <?= $view->getSliderTime();?>
            "slidesToShow": 1,
            "slidesToScroll": 1,
            "prevArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-prev\">'. get_svg_symbol('icon_ui_arrow-left', true) .'</button>"':'false'; ?>,
            "nextArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-next\">'. get_svg_symbol('icon_ui_arrow-right', true) .'</button>"':'false'; ?>,
            "responsive": [
                {
                    "breakpoint": <?= (int)get_breakpoint('medium-portrait'); ?>,
                    "settings": {
                        "slidesToShow": 2,
                        "slidesToScroll": 2
                    }
                },
                {
                    "breakpoint": <?= (int)get_breakpoint('large'); ?>,
                    "settings": {
                        "slidesToShow": <?= $view->getSlideAmount()?>,
                        "slidesToScroll": <?= $view->getSlideAmount('scroll')?>
                    }                                }
                }
            ]
        }' <?php endif; ?>>

        <?php if( $query->have_posts() ) while( $query->have_posts() ): $query->the_post();?>


        <a href="<?= Product::url(); ?>" class="ac_item" data-s-amount-item <?= $view->elementAnimation(); ?>>
            <div class="ac_item_container" data-border-bottom>
                 <?php if( Product::isSale() ) : ?>
                <div class="ac_item_content_label">
                    Actie
                </div>
                <?php endif; ?>
                <?php 
                    $imgId = Product::thumbId(); 
                    if(!is_null($imgId) ):
                ?>
                <div class="ac_item_image-container">
                    <figure class="ac_item_image a_contain <?= Image::alignment( $imgId ); ?>" data-interchange="[<?= Image::url( $imgId, '600w') ?>, small]" style="background-image: url(<?= Image::url( $imgId, '600w') ?>)"></figure>
                </div>
                <?php endif; ?>
                
                <div class="ac_item_content">
                    <div class="ac_item_icon">
                        <?= get_svg_symbol('icon_ui_arrow-right'); ?>
                    </div>
                    <div class="ac_item_content_copy">

                        <div class="ac_item_content_copy-above">
                            <div class="ac_item_content_price">
                                <?php if( Product::isSale() ) : ?>
                                    <div class="ac_item_content_price_before"><?= __('van','abeltheme' ) . ' ' . Product::oldPrice(); ?></div>
                                <?php endif; ?>

                                <div class="ac_item_content_price_current"><?= (Product::isSale()?__('| voor','abeltheme' ):'') . ' ' . Product::price(); ?></div>
                            </div>
                        </div>
                        <div class="ac_item_content_title">
                            <?= Product::title(); ?>
                        </div>
                         <div class="ac_item_content_copy-below">
                            <?= Product::description(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <?php
        endwhile; 
        ?>

    </div>
    <?php $view->end();?>

</section>