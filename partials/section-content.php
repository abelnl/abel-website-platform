<section data-s-type="<?= $view->type('section-content');?>" data-s-id="<?= $view->s_id();?>" class="<?= $view->class('ac_content' );?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>
    
    <?= $view->title(); ?>

    <div class="ac_content_container a_collapsed <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" <?= $view->elementAnimation(); ?>>
        <div class="a_bg_brand_inherit a_padding-top-inherit a_padding-bottom-inherit">

            <div class="ac_content_flex">
                
                <?= $view->columns();?>
    
            </div>
        </div>
    </div>
    
    <?php $view->end();?>
</section>