<?php
    /*

        MAP

    */
    
    $local_labels = true;

    //light
    $text_color = '#333333';
    $landfill_border = '#aaaaaa';
    $landfill = '#fafafa';
    $water = '#f5f5f5';
    $highway = '#d1d1d1';
    $road = '#e1e1e1';
    $street = '#f1f1f1';

    //dark
    // $text_color = '#cccccc';
    // $landfill_border = '#999999';
    // $landfill = '#222222';
    // $water = '#333333';
    // $highway = '#444444';
    // $road = '#373737';
    // $street = '#353535';
    // 
    $latLng = $column->getField('latlng');
?>



<section data-s-type="<?= $view->type('maps');?>" data-s-id="<?= $view->s_id();?>" class="<?= $view->class('ac_content' );?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>

    <?= $view->title();?>
        
    <div id="map" class="ac_maps_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" <?= $view->elementAnimation(); ?>></div>
    
    <?php $view->end();?>
</section>
<script>
    function initMap() {
   
        var location = {lat: <?= $latLng['lat'];?>, lng: <?= $latLng['lng'];?>};
        
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: <?= $column->getField('zoomlevel')?>,
            center: location,
            disableDefaultUI: true,
            zoomControl: <?= $column->getField('showcontrols')?>,
            scrollwheel: false,
            gestureHandling: 'cooperative',
            styles: [
            {
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "<?= $landfill; ?>"
                }
                ]
            },
            {
                "elementType": "geometry.fill",
                "stylers": [
                {
                    "color": "<?= $landfill; ?>"
                }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "geometry.stroke",
                "stylers": [
                {
                    "color": "<?= $landfill_border; ?>"
                },
                {
                    "visibility": "on"
                }
                ]
            },
            {
                "elementType": "labels",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
    <?php      if($local_labels):  ?>
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text",
                "stylers": [
                {
                    "color": "<?= $text_color; ?>"
                },
                {
                    "visibility": "simplified"
                }
                ]
            },
    <?php      endif; ?>
            {
                "featureType": "poi",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                {
                    "color": "<?= $street; ?>"
                }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [
                {
                    "color": "<?= $road; ?>"
                }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                {
                    "color": "<?= $highway; ?>"
                }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                {
                    "color": "<?= $water; ?>"
                }
                ]
            }
            ]
        });
       var contentString = '<div id="content">'+
            '<div id="bodyContent">'+
                <?= $column->getContent() ?>
            +'</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 400
        });

        var marker = new google.maps.Marker({
            position: location,
            map: map,
            title: 'Uluru (Ayers Rock)'
        });
        <?php if( (string)$column->getField( 'showinfowindow' ) == 'true' ):?>
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });

        // open infowindow
        infowindow.open(map, marker);
        <?php endif;?>
        // offset the map to fit content
        map.panBy(0,-70);
    }


</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQ7MWd5CKgDNqq66ousWKfpOR3vo413NM&callback=initMap"></script>