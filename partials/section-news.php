<?php
    use \Abel\Helpers\Item;
    use Abel\Helpers\Image;
    use \Cuisine\Utilities\Url;
    use \Cuisine\Wrappers\Script;

    /*  

        ITEMS

    */
?>

<section data-s-type="<?= $view->type('news');?>" data-s-id="<?= $view->s_id();?>" data-s-amount="<?= $view->amount();?>" class="<?= $view->class('ac_content');?> <?php if( $view->isSlider() ) echo 'v_is-slider';?> <?if ( $view->showNavigation() ) echo 'v_has-dots'; ?>" id="<?= $view->id();?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>

    <?= $view->title(); ?>

    <div class="ac_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" <?php if( $view->isSlider() ): ?>data-init-slick data-slick='{
            "mobileFirst": true,
            "dots": <?= $view->showNavigation()?>,
            "slidesToShow": 1,
            "slidesToScroll": 1,
            <?= $view->getSliderTime();?>
            "prevArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-prev\">'. get_svg_symbol('icon_ui_arrow-left', true) .'</button>"':'false'; ?>,
            "nextArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-next\">'. get_svg_symbol('icon_ui_arrow-right', true) .'</button>"':'false'; ?>,
            "responsive": [
                {
                    "breakpoint": <?= (int)get_breakpoint('medium-portrait'); ?>,
                    "settings": {
                        "slidesToShow": 2,
                        "slidesToScroll": 2
                    }
                },
                {
                    "breakpoint": <?= (int)get_breakpoint('large'); ?>,
                    "settings": {
                        "slidesToShow": <?= $view->getSlideAmount()?>,
                        "slidesToScroll": <?= $view->getSlideAmount('scroll')?>            
                    }
                }
            ]
        }' <?php endif; ?>>

        <?php if( $query->have_posts() ) while( $query->have_posts() ): $query->the_post();?>
        <?php
        if ( $view->showCollectionLinks() ):
        ?>
        <a href="<?= Item::link();?>" class="ac_item" data-s-amount-item <?= $view->elementAnimation(); ?>>
        <?php
        else:
        ?>
        <div class="ac_item" data-s-amount-item <?= $view->elementAnimation(); ?>>
        <?php
        endif;
        ?>
            <div class="ac_item_container" data-border-bottom>
                <?php 
                    $imgId = Item::thumbId(); 
                    if(!is_null($imgId) ):
                ?>
                <div class="ac_item_image-container">
                    <figure class="ac_item_image <?= Image::alignment( $imgId ); ?>" data-interchange="[<?= Image::url( $imgId, '600w') ?>, small]" style="background-image: url(<?= Image::url( $imgId, '600w') ?>)"></figure>
                </div>
                <?php endif; ?>
                
                <div class="ac_item_content">
                    <div class="ac_item_icon">
                        <?= get_svg_symbol('icon_ui_arrow-right'); ?>
                    </div>
                    <div class="ac_item_content_copy">
                     
                        <div class="ac_item_content_icon_container">
                            <div class="ac_item_content_icon">
                                <?= get_svg_symbol(get_random_content('service-icon')); ?>
                            </div>                            
                        </div>
                       
                        <div class="ac_item_content_copy-above">
                            <?= Item::date();?>
                        </div>
                        <div class="ac_item_content_title">
                            <?= Item::title();?>
                        </div>
                         <div class="ac_item_content_copy-below">
                            <?= Item::cat();?>
                        </div>
                    </div>
                </div>
            </div>
            
        <?php
        if ( $view->showCollectionLinks() ):
        ?>
        </a>
        <?php
        else:
        ?>
        </div>
        <?php
        endif;
        ?>

        <?php endwhile;?>

    </div>

    <?php $view->end();?>

</section>