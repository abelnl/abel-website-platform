<?php

    use Abel\Wrappers\Header;

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/public/assets/img/favicon.ico" type="image/x-icon">

    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri() ?>/public/assets/css/app.css">

    <script>
        
        var HOME_URL = "<?= home_url(); ?>";
        var TEMPLATE_DIR_URI = "<?= get_stylesheet_directory_uri(); ?>";

        //fuoc solution 
        document.write(
            "<style type='text/css'>" +
            "[data-anime-elem] { opacity: 0; }" +
            "</style>"
        );
   
    </script>
    <?= Header::tagManager();?>
  </head>
  <body <?php body_class(); ?>>
  
