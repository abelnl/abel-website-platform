<?php
    
    use \Cuisine\Wrappers\Script;
    use \Cuisine\Utilities\Url;
    use \Abel\Helpers\Image;
    
    /*

        IMAGE GALLERY 

    */

    $media = $column->getMedia();
    
?>
<section data-s-type="<?= $view->type('image-gallery');?>" data-s-id="<?= $view->s_id();?>" data-s-amount="<?= $view->getSlideAmount()?>" class="<?= $view->class('ac_content' );?> <?php if( $view->isSlider() ) echo 'v_is-slider';?> <?if ( $view->showNavigation() ) echo 'v_has-dots'; ?>" id="<?= $view->id();?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>
    <?php if( $column->hasLightbox() ):?>
         data-overlay="overlay-<?= $view->id();?>" 
    <?php endif;?>
>   
<?= $view->title(); ?>

    <div class="ac_image-gallery_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" <?php if( $view->isSlider() ): ?>data-slick='{
            "mobileFirst": true,
            "dots": <?= $view->showNavigation()?>,
            "slidesToShow": 1,
            "slidesToScroll": 1,
            "centerMode": true,
            "focusOnSelect": true,
            "prevArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-prev\">'. get_svg_symbol('icon_ui_arrow-left', true) .'</button>"':'false'; ?>,
            "nextArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-next\">'. get_svg_symbol('icon_ui_arrow-right', true) .'</button>"':'false'; ?>,
            "responsive": [
                {
                    "breakpoint": <?= (int)get_breakpoint('medium-portrait'); ?>,
                    "settings": {
                        "slidesToShow": 1,
                        "slidesToScroll": 1
                    }
                },
                {
                    "breakpoint": <?= (int)get_breakpoint('medium'); ?>,
                    "settings": {
                        "slidesToShow": <?= $view->getSlideAmount()?>,
                        "slidesToScroll": <?= $view->getSlideAmount('scroll')?>            
                    }
                }
            ]
        }' data-init-slick <?php endif; ?>>
    
        <?php $index = 0;?>
        <?php foreach( $media as $item ):?>
            <?php Image::set( $item );?>
            <<?= ( $column->hasLightbox() ? 'a href="#"' : 'div' );?> class="ac_image-gallery_item" data-s-amount-item="<?= $index;?>">
                <div class="ac_image-gallery_item_container" <?= $view->elementAnimation(); ?>>
                    <div class="ac_image-gallery_item_image_container">
                        <figure class="ac_image-gallery_item_image <?= Image::alignment(); ?>" data-interchange="[<?= Image::url( null, '600w') ?>, small], [<?= Image::url( null, '1000w') ?>, medium], [<?= Image::url( null, '1000w') ?>, large]" style="background-image: url(<?= Image::url( null, '1000w') ?>)"></figure>
                    </div>
                    <?php if( Image::hasTitle() ):?>
                    <div class="ac_image-gallery_item_content">
                        <div class="ac_image-gallery_item_content_inner">
                           <?= Image::title(); ?>
                        </div>
                    </div>
                    <?php endif;?>
                </div>
            </<?= ( $column->hasLightbox() ? 'a' : 'div' );?>>
    
        <?php $index++;?>
        <?php endforeach;?>
    </div>

    <?php $view->end();?>
</section>

<?php if( $column->hasLightbox() ):?>
    <?= Image::lightbox( $media, $view );?>
<?php endif;?>