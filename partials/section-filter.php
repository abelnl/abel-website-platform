<?php 
	$terms = $column->getTerms();
	$targetSection = $column->getSection();
	$collection = $view->id( $targetSection );

?>
<section data-s-type="<?= $view->type();?>" class="<?= $view->class('ac_content');?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>

	<?= $view->title(); ?>
	<div class="ac_content_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>"> 

        <?php if( !is_null( $column->getTitle() ) ):?>
        <div class="ac_cta_column">
            <?php $column->theTitle();?>
        </div>
        <?php endif;?>

		<div class="filter-items" data-tax="<?= $column->getField( 'taxonomy' );?>" data-collection-id="<?= $collection ?>" data-section-id="<?= $targetSection->id;?>" data-post-id="<?= $targetSection->post_id;?>" data-message="<?= $column->getField( 'message', 'Geen berichten meer gevonden' );?>">


		<?php if( $column->getField( 'show_search', 'false' ) == 'true' ):?>
		<div class="input-group search">
			<input type="text" name="s" id="search-query" placeholder="Zoeken">
			<div class="input-group-button">
				<input type="submit" class="submit button" value="Zoeken" id="submit-search">
			</div>
		</div>
		<?php endif;?>

	
		<?php if( $column->getField('taxonomy') !== 'none' ):?>
		<?php $terms = $column->getTerms();?>
		<?php if( !empty( $terms ) ):?>

			<button class="button v_smallest filter active" data-filter="all">
				Alles
			</button>		
		
			<?php foreach( $terms as $term ):?>
				<button class="button v_smallest filter" data-filter="<?= $term->term_id;?>"><?= $term->name;?></button>
			<?php endforeach;?>
		<?php endif;?>
		<?php else:?>
		<input class="filter" data-filter="all" type="hidden"/>
		<?php endif;?>
		</div>

	</div>
</section>