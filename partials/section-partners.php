<?php
    
    use \Cuisine\Wrappers\Script;
    use \Cuisine\Utilities\Url;
    use \Abel\Helpers\Brand;
    use \Abel\Helpers\Image;
    use \Abel\Helpers\Pagination;
    use Abel\Front\Settings;
    /*

        BRANDS ALT

    */
?>

<section data-s-type="<?= $view->type();?>" data-s-id="<?= $view->s_id();?>" data-s-amount="<?= $view->amount(2);?>" class="<?= $view->class('ac_content');?> <?php if( $view->isSlider() ) echo 'v_is-slider';?> <?php if ( $view->showNavigation() != 'false' ) echo 'v_has-dots'; ?>" id="<?= $view->id();?>"  <?= $view->sectionAnimation(); ?>>
    <?= $view->title(); ?>
    <div class="ac_partners_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" <?php if( $view->isSlider() ): ?>data-slick='{
            "mobileFirst": true,
            "dots": <?= $view->showNavigation()?>,
            "slidesToShow": 1,
            "slidesToScroll": 1,
            <?= $view->getSliderTime();?>
            "prevArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-prev\">'. get_svg_symbol('icon_ui_arrow-left', true) .'</button>"':'false'; ?>,
            "nextArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-next\">'. get_svg_symbol('icon_ui_arrow-right', true) .'</button>"':'false'; ?>,
            "responsive": [
                {
                    "breakpoint": <?= (int)get_breakpoint('medium-portrait'); ?>,
                    "settings": {
                        "slidesToShow": 2,
                        "slidesToScroll": 2
                    }
                },
                {
                    "breakpoint": <?= (int)get_breakpoint('large'); ?>,
                    "settings": {
                        "slidesToShow": <?= $view->getSlideAmount()?>,
                        "slidesToScroll": <?= $view->getSlideAmount()?>            
                    }
                }
            ]
        }' data-init-slick <?php endif; ?>>
    

        <?php if( $query->have_posts() ) while( $query->have_posts() ): $query->the_post();?>
        <?php
        $link = Brand::externalLink();
        if ( $view->showCollectionLinks() || $link ):
            if( !$link ) {
                $link = Brand::link();
            }
        ?>
            <a href="<?= $link ?>" class="ac_partners_item<?= Brand::logoClass();?>" <?= Brand::externalLink()?'target="_blank"':'' ?> data-anime-elem data-s-amount-item>
        <?php else: ?>
            <div class="ac_partners_item<?= Brand::logoClass();?>"  data-anime-elem data-s-amount-item>
        <?php endif; ?>
        
            <div class="ac_partners_item_container" data-border-bottom>
                <div class="ac_partners_item_content">
                    <div class="ac_partners_item_logo_container">
                        <?php 
                            $useAltLogos = Settings::get( 'partner_logo_alt', false );
                            if ( (string)$useAltLogos == 'true' )
                                $logo = Brand::logo(true);
                            else 
                                $logo = Brand::logo();
                        ?>
                        <div class="ac_partners_item_logo" style="background-image: url('<?= $logo; ?>');"></div>
                    </div>
                    <div class="ac_partners_item_description">
                        <?= Brand::title() ?>
                    </div>
                </div>
            </div>
        <?php if ( $view->showCollectionLinks() || $link ):?>
            </a>
        <?php else: ?>
            </div>
        <?php endif; ?>

        <?php endwhile; ?>

    </div>

    <?php $view->end();?>

</section>