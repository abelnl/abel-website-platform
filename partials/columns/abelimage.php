<?php 
	use Abel\Helpers\Image;

	$img = $column->getField( 'image' );
	$imgId = $img['img-id'];
	Image::set($img);
	$img = Image::url( null, $column->getField( 'size' ) );
	if( is_null( $img ) )
		$img = Image::url( null , 'full' );
?>
<img src="<?= $img;?>" alt="<?= get_the_title( $imgId );?>"/>