<?php

    /*

        LIST BUTTONS

    */

    use Abel\Helpers\Icon;
    use Abel\Helpers\Social;
    use Abel\Helpers\Image;
    use Abel\Helpers\Button;

?>
<section data-s-type="<?= $view->type(); ?>" data-s-id="<?= $view->s_id();?>" class="<?= $view->class('ac_content');?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>
    <?= $view->title(); ?>
    <div class="ac_listbuttons_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>">
        <div class="ac_listbuttons_container_inner">
            <?php if( !is_null( $column->getTitle() ) ):?>

                <?php $column->theTitle();?>

            <?php endif;?>

            <?php if( $column->hasButtons() ):?>

                <?php 
                foreach( $column->getField( 'buttons' ) as $button ): 
                    $button = $button['button'];
                ?>
                <a href="<?= $button['link'];?>"  <?= (array_key_exists('target', $button) && ($button['target'] || $button['target'] == 'true') && $button['target'] != 'false')?'target="_blank"':'' ?> class="<?= Button::getClass( $button );?>">
                    <?= $button['text'] ?>
                    <?php if( $button['icon'] != '' && $button['icon'] != 'none' ): ?>
                    <span class="svg-container">
                        <?= Icon::get( $button['icon'] ); ?>
                    </span>
                    <?php endif; ?>
                </a>    
                <?php endforeach ?>

            <?php endif; ?>
        </div>
    </div> 

    <?php $view->reset();?>
    <?php $view->divider(); ?>
    <?php $view->backdrop();?>
</section>