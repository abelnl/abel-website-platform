<?php
    use Cuisine\Wrappers\Script;
    use \Cuisine\Utilities\Url;

    $id = str_replace( '-','_', $view->id() );
    $feed = $column->getFeed();
?>

<section data-s-type="<?= $view->type('products');?>" data-s-amount="<?= $view->amount();?>" data-s-id="<?= $view->s_id();?>" class="<?= $view->class('ac_content');?>" id="<?= $view->id();?>" data-productfeed="true" data-anime-type="transform-in">
    <?= $view->title(); ?>
    <div class="ac_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" id="ac_products_feed">
        <div class="ac_products_feed_container" id="ac_products_feed_<?= $id;?>"></div>
    </div>
    <script>
        window.feed_<?= $id ?> = '<?= $feed;?>';
    </script>
</section>