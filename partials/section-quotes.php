<?php
    use \Cuisine\Wrappers\Script;
    use \Cuisine\Utilities\Url;
    use Abel\Helpers\Icon;
    use Abel\Helpers\Image;

    /*  

        ITEMS

    */
   
    $sections = [ $section ];
    if( isset( $section->sections ) ){
        $container = $section;
        $sections = $container->sections->all();
    }
?>

<section data-s-type="<?= $view->type('quotes');?>" data-s-id="<?= $view->s_id();?>" data-s-amount="1" class="<?= $view->class('ac_content');?> <?php if( $view->isSlider() ) echo 'v_is-slider';?> <?if ( $view->showNavigation() ) echo 'v_has-dots'; ?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>

    <?= $view->title(); ?>

    <div class="ac_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" <?php if($view->isSlider()): ?>data-init-slick data-slick='{
            "mobileFirst": true,
            "dots": <?= $view->showNavigation()?>,
            "slidesToShow": 1,
            "slidesToScroll": 1,
            <?= $view->getSliderTime();?>
            "adaptiveHeight": true,
            "prevArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-prev\">'. get_svg_symbol('icon_ui_arrow-left', true) .'</button>"':'false'; ?>,
            "nextArrow": <?= $view->showNavigationArrows()?'"<button type=\"button\" class=\"slick-next\">'. get_svg_symbol('icon_ui_arrow-right', true) .'</button>"':'false'; ?>,
            "responsive": [
                {
                    "breakpoint": <?= (int)get_breakpoint('medium-portrait'); ?>,
                    "settings": {
                        "slidesToShow": 1,
                        "slidesToScroll": 2
                    }
                },
                {
                    "breakpoint": <?= (int)get_breakpoint('large'); ?>,
                    "settings": {
                        "slidesToShow": <?= $view->getSlideAmount()?>,
                        "slidesToScroll": <?= $view->getSlideAmount('scroll')?>            
                    }
                }
            ]
        }' <?php endif; ?>>

        <?php 

        foreach( $sections as $section ):
            $quote = $section->getColumn();
            $image = $quote->getField( 'avatar' );
            if( $image['img-id'] == '' )
                $image = null;

            $bgImageId = $view->getBackgroundId( $section );        
        ?>

        <div class="ac_item" data-s-amount-item <?= $view->elementAnimation(); ?>>
            <div class="ac_item_container" data-border-bottom>
                
                <?php 
                   if( !is_null( $bgImageId ) ):
                ?>
                <div class="ac_item_image-container">
                    <figure class="ac_item_image <?= $view->getBackgroundAlignment( $section ); ?> a_medium_cover-ct" data-interchange="[<?= Image::url( $bgImageId, '600w') ?>, small], [<?= Image::url( $bgImageId, '1000w') ?>, medium], [<?= Image::url( $bgImageId, '1600w') ?>, large]" style="background-image: url(<?= Image::url( $bgImageId, '1600w') ?>)"></figure>
                </div>
                <?php endif;?>

                <div class="ac_item_content">
                    
                    <div class="ac_item_content_copy">
                        <div class="ac_item_content_copy-above">

                        </div>
                        <div class="ac_item_content_title">
                            <div class="ac_item_content_title_quote-left">
                                <?= Icon::get('icon_ui_quote-left'); ?>
                            </div>
                            <?= $quote->getField('quote'); ?>
                            <div class="ac_item_content_title_quote-right">
                                <?= Icon::get('icon_ui_quote-right'); ?>
                            </div>
                        </div>
                         <div class="ac_item_content_copy-below">

                            <div class="ac_item_author">
                             <?php 
                             if( !is_null( $image ) ):
                                $imgId = $image['img-id'];
                             ?>
                                <div class="ac_item_avatar_container">
                                    <figure class="ac_item_avatar_image <?= Image::alignment( $imgId ); ?>" data-interchange="[<?= Image::url( $imgId, '600w') ?>, small], [<?= Image::url( $imgId, '1000w') ?>, medium], [<?= Image::url( $imgId, '1600w') ?>, large]" style="background-image: url(<?= Image::url( $imgId, '1600w') ?>)"></figure>
                                </div>
                            <?php endif; ?>
                                
                                <div class="ac_item_author_name">
                                    <?= $quote->getField('author'); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php endforeach; //end section loop ?>

    </div>

    <?php $view->end();?>
</section>