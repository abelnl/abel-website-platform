<?php
    /*

        IMAGE GALLERY GRID

    */
    
    use Abel\Helpers\Image;

?>

<section data-s-type="<?= $view->type( 'image-gallery-grid' );?>" class="<?= $view->class('ac_content');?>" id="<?= $view->id();?>"  <?= $view->sectionAnimation(); ?>
    <?php if( $column->hasLightbox() ):?>
         data-overlay="overlay-<?= $view->id();?>" 
    <?php endif;?>
>   

    <?php 
    echo $view->title();

    $total = 0;
    $base = 1;

    for( $i = 1; $i <= ceil( count( $items ) / 3 ); $i++ ):
            $a = $base;
            $b = $base+1;
            $c = $base+2;

            $base = $c + 1;
    ?>
    <div class="ac_image-gallery_grid_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>">
        
        <div class="ac_image-gallery_grid_outer-column">
            <?php if( isset( $items[ $a ] ) ):?>
            <div class="ac_image-gallery_grid_column">
                <?php foreach( $items[ $a ] as $item ):?>
                    <?= Image::gridItem( $item, $section, $total ); $total++;?>
                <?php endforeach; ?>
            </div>
            <?php endif;?>
            <?php if( isset( $items[ $b ] ) ):?>
            <div class="ac_image-gallery_grid_column">
                <?php foreach( $items[ $b ] as $item ):?>
                    <?= Image::gridItem( $item, $section, $total ); $total++;?>
                <?php endforeach; ?>
            </div>
            <?php endif;?>
        </div>
        <?php if( isset( $items[ $c ] )  ):?>
        <div class="ac_image-gallery_grid_outer-column">
            <div class="ac_image-gallery_grid_column">
            <?php foreach( $items[ $c ] as $item ):?>
                <?= Image::gridItem( $item, $section, $total ); $total++;?>
            <?php endforeach; ?>
            </div>
        </div>
        <?php endif;?>
    </div>
    <?php endfor;?>
    <?php $view->end();?>

</section>


<?php if( $column->hasLightbox() ):?>
    <?= Image::lightbox( $column->getMedia(), $view );?>
<?php endif;?>