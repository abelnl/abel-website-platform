<?php
    /*

        VAC SECTION || VISUAL AND CONTENT

    */
    
        use Abel\Helpers\Image;

?>

<section data-s-type="<?= $view->type();?>" data-s-id="<?= $view->s_id();?>" class="<?= $view->class('ac_content');?>" <?= $view->sectionAnimation(); ?>>

    <?= $view->title(); ?>

    <div class="ac_vac_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>" >

        <?php 

            $firstColumn = $section->getColumn();

            $class = 'v_text-right';
            if( $firstColumn->type == 'content' )
                $class = 'v_text-left';
        ?>

        <div class="ac_vac_item <?= $class;?>">
            <div class="ac_vac_item_container">
            <?php foreach( $section->columns as $column ):?>
                <?php if( $column->type == 'content' ):?>
                <div class="ac_vac_item_content">
                    <div class="ac_vac_item_content_inner">

                        <?php if( !is_null( $column->getTitle() ) ):?>
                            <?php $column->theTitle();?>
                        <?php endif;?>

                        <?= $column->getField( 'content' );?>

                    </div>
                </div>
                <?php elseif( $column->getField( 'id' ) != '' ): ?>

                <?php $id = $column->getField('id'); ?>
                <div class="ac_vac_item_visual">
                    <div class="ac_backdrop"> 
                        <div class="ac_backdrop_image-container a_bg_brand_primary">
                            <figure class="ac_backdrop_image <?= Image::alignment( $id ); ?>" data-interchange="[<?= Image::url( $id, '600w') ?>, small], [<?= Image::url( $id, '1000w') ?>, medium], [<?= Image::url( $id, '1000w') ?>, large]" style="background-image: url(<?= Image::url( $id, '1000w') ?>)"></figure>
                        </div>
                    </div>
                </div>
                <?php endif;?>
            <?php endforeach;?>
            </div>
        </div>


    </div>

    <?php $view->end();?>
    
</section>