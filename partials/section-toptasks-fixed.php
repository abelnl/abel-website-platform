<?php
use \Abel\Helpers\Icon;
use Abel\Front\Settings;

$buttons = Settings::get( 'toptasks' );
if ( $buttons && is_array($buttons) && count( $buttons[0] ) > 0 && $buttons[0]['toptaskbutton']['link'] != '' ):
?>
<section data-s-type="toptasks-fixed" class="ac_content" id="topttaks-fixed">

    <div class="ac_toptasks_fixed_container">
        <div class="ac_toptasks_fixed_container_inner a_max-width-1000">
        
        <?php 
        $buttons = Settings::get( 'toptasks' );
        foreach( $buttons as $button ):
            $button = $button['toptaskbutton'];
        ?>
            <a href="<?= $button['link'];?>" <?= (array_key_exists('target', $button) && ($button['target'] || $button['target'] == 'true'))?'target="_blank"':'' ?> class="ac_toptasks_fixed_item" data-s-amount-item>

                <div class="ac_toptasks_fixed_item_container">
                    <div class="ac_toptasks_fixed_item_icon-container">

                        <?php if( $button['icon'] != '' && $button['icon'] != 'none' ): ?>
                            <div class="ac_toptasks_fixed_item_icon">
                                <?= Icon::get( $button['icon'] ); ?>
                            </div>
                        <?php endif; ?>
                        
                    </div>
                    <div class="ac_toptasks_fixed_item_content">
                        <div class="ac_toptasks_fixed_item_content_title"><?= $button['text']; ?></div>
                        <div class="ac_toptasks_fixed_item_content_btn-container"><?= $button['text']; ?></div>
                    </div>
                </div>
            </a>
        <?php endforeach; ?>
        </div>
    </div>
</section>
<?php
endif;
?>