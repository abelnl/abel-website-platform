<?php 
    use Abel\Helpers\Image;

    Image::set( $item );
?>

    <<?php if($lightbox == true) { echo 'a href="#"'; } else { echo 'div'; } ?> class="ac_image-gallery_grid_item"  data-s-amount-item="<?= $index;?>" data-anime-elem>
        <div class="ac_image-gallery_grid_item_container">
            <div class="ac_image-gallery_grid_item_image_container">
                <figure class="ac_image-gallery_grid_item_image <?= Image::alignment(); ?>" data-interchange="[<?= Image::url( null, '600w') ?>, small], [<?= Image::url( null, '1000w') ?>, medium], [<?= Image::url( null, '1000w') ?>, large]" style="background-image: url(<?= Image::url( null, '1000w') ?>)"></figure>
            </div>
            <?php if( Image::hasTitle() ):?>
            <div class="ac_image-gallery_grid_item_content">
                <div class="ac_image-gallery_grid_item_content_inner">
                    <?= Image::title() ?>
                </div>
            </div>
            <?php endif;?>
        </div>
    </<?php if($lightbox == true) { echo 'a'; } else { echo 'div'; } ?>>