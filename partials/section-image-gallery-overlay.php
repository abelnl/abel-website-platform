<?php

    use Abel\Helpers\Image;
    use Cuisine\Utilities\Sort;

    $media = Sort::byField( $media, 'position' );

?>
<button class="ac_image-gallery_full-screen_close" id="close-overlay-<?= $view->id();?>">
    <?= get_svg_symbol('icon_ui_close'); ?>
</button>

<div data-s-type="image-gallery_full-screen" class="ac_image-gallery_full-screen" id="overlay-<?= $view->id();?>" >
    <div class="ac_image-gallery_full-screen_container" data-slick='{
        "mobileFirst": true,
        "dots": false,
        "slidesToShow": 1,
        "slidesToScroll": 1,
        "focusOnSelect": true,
        "infinite": true,
        "prevArrow": "<button type=\"button\" class=\"slick-prev\"><?= get_svg_symbol('icon_ui_arrow-left', true); ?></button>",
        "nextArrow": "<button type=\"button\" class=\"slick-next\"><?= get_svg_symbol('icon_ui_arrow-right', true); ?></button>"
        }'>
        <?php $index = 0;?>
        <?php foreach( $media as $item ): ?>
            <?php Image::set( $item );?>
            <div class="image-gallery_full-screen_item" data-s-index="<?= $index;?>">
                <div class="image-gallery_full-screen_item_container">
                    <div class="image-gallery_full-screen_item_image_container">
                        <img src="<?= Image::url(null, '1600w'); ?>" class="image-gallery_full-screen_item_image <?= Image::alignment(); ?>" alt="">
                    </div>
                    <?php if( Image::hasTitle() ):?>
                    <div class="image-gallery_full-screen_item_content">
                        <div class="image-gallery_full-screen_item_content_inner">
                            <h3><?= Image::title();?></h3>
                            <?= Image::content();?>
                        </div>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        <?php $index++;?>
        <?php endforeach;?>
    </div>
</div>
<?php $view->reset();?>
<div class="ac_image-gallery_full-screen_background" id="bg-overlay-<?= $view->id();?>"></div>
