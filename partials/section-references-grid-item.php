<?php 
    
    use Abel\Helpers\Reference;
    use Abel\Helpers\Icon;
    use Abel\Helpers\Image;

    Reference::set( $item );
?>
<?php
if ( $view->showCollectionLinks() ):
?>
<a href="<?= Reference::link();?>" class="ac_item" data-s-amount-item data-anime-elem>
<?php
else:
?>
<div class="ac_item" data-s-amount-item data-anime-elem>
<?php
endif;
?>
    <div class="ac_item_container" data-border-bottom>
        <?php 
            $imgId = Reference::thumbId(); 
            if(!is_null($imgId) ):
        ?>
        <div class="ac_item_image-container">
            <figure class="ac_item_image <?= Image::alignment( $imgId ); ?>" data-interchange="[<?= Image::url( $imgId, '600w') ?>, small]" style="background-image: url(<?= Image::url( $imgId, '600w') ?>)"></figure>
        </div>
        <?php endif; ?>
        <div class="ac_item_content">
            <div class="ac_item_icon">
                <?= Icon::get('icon_ui_arrow-right'); ?>
            </div>
            <div class="ac_item_content_copy">
                <div class="ac_item_content_copy-above">
                    <?= Reference::location(); ?>
                </div>
                <div class="ac_item_content_title">
                    <?= Reference::title(); ?>
                </div>
                    <div class="ac_item_content_copy-below">
                    <?= Reference::description(); ?>
                </div>
            </div>
        </div>
    </div>
<?php
if ( $view->showCollectionLinks() ):
?>
</a>
<?php
else:
?>
</div>
<?php
endif;
?>