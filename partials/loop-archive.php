<li class="ac_list-search_item">
	<a href="<?php the_permalink(); ?>" class="ac_list-search_item_link">
		<div class="ac_list-search_item_content">
			<div class="ac_list-search_item_title"><?php the_title(); ?></div>
		</div>
	</a>
</li>
