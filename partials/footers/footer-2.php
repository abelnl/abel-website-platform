<?php
    use Abel\Wrappers\Footer;
?>
<section data-f-type="footer-2">
    
    <div class="ac_footer_primary">
        <div class="ac_footer_primary_container">


            <div class="ac_footer_primary_column">
               <?= Footer::content( 'footer-2', 1 )->get();?>
            </div>
            <div class="ac_footer_primary_column">
               <?= Footer::content( 'footer-2', 2 )->get();?>
            </div>
            <div class="ac_footer_primary_column">
               <?= Footer::content( 'footer-2', 3 )->get();?>
            </div>
            <div class="ac_footer_primary_column">
               <?= Footer::content( 'footer-2', 4 )->get();?>
            </div>
        

        </div>
    </div>

    
    <div class="ac_footer_secondary">
        <div class="ac_footer_secondary_container">

            <div class="ac_footer_secondary_column">
               <?= Footer::content( 'footer-2', 5 )->get();?>                
            </div>
            
            <div class="ac_footer_secondary_column">
                <?= Footer::content( 'footer-2', 6 )->get();?>
            </div>
                
        </div>
    </div>

</section>