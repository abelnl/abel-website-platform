<?php

    use Abel\Helpers\Icon;
    use Abel\Helpers\Social;

    $class = 'v_smallest';
    switch( $iconSize ){

        case 'middle':
            $class = 'v_smaller';
            break;
        case 'big':
            $class = 'v_big';
            break;
    }
?>
<div class="ac_socials <?= $class ?>"> 
    <?= $title; ?>            
    <?php 
    $social_description = Social::social_description( );
    if ( $social_description && $social_description != '' ):
    ?>
    <span class="ac_socials_text">
        <?= $social_description ?>
    </span>
    <?php endif; ?>
    <?php foreach( Social::all() as $key => $label ):?>
        <?php if( Social::hasLink( $key ) ):?>
        <a href="<?= Social::link( $key );?>" class="ac_socials_link" target="_blank">
            <?php 
            $description = Social::description( $key );
            if ( $description && $description != '' ):
            ?>
            <span class="ac_socials_link_text">
                <?= $description ?>
            </span>
            <?php endif; ?>
            <span class="ac_socials_link_icon">
                <?= Icon::get('icon_ui_'. $key ); ?>
            </span>
            <!--span class="ac_socials_link_label">
                <?php //$label ?>
            </span-->
        </a>
        <?php endif;?>
    <?php endforeach;?>
</div>