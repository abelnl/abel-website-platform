<?= $title; ?>            
<ul class="footer-post-list">
	<?php if( $query->have_posts() ) while( $query->have_posts() ): $query->the_post();?>
	<li><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></li>
	<?php endwhile;?>
</ul>