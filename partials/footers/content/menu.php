<?= $title; ?>            
<?php wp_nav_menu([ 
	'menu' => $menuId,
	'items_wrap' => 
		'<ul id="%1$s" class="%2$s">%3$s</ul>',
	'depth' => 1
]);?>