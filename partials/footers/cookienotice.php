<?php
	use Abel\Front\Settings;
?>

    <section data-s-type="cookie-message" class="ac_content">
		<div class="ac_content_container a_max-width-1000">
			<div class="ac_content_flex">
				<div class="ac_content_2-3">
					<h2><?= Settings::get( 'cookie_title' ); ?></h2>
					<p><?= Settings::get( 'cookie_message' ); ?></p>
				</div>
				<div class="ac_content_1-3">
					<button class="button" data-toggle-cookie-message=""><?= Settings::get( 'cookie_buttontext' ); ?></button>
				</div>
			</div>
		</div>
	</section>