<?php
    use Abel\Wrappers\Footer;
?>
<section data-f-type="footer-1">
    
    <div class="ac_footer_primary">
        <div class="ac_footer_primary_container">

            <div class="ac_footer_primary_column">
               <?= Footer::content( 'footer-1', 1 )->get();?>
            </div>

            <div class="ac_footer_primary_column">
               <?= Footer::content( 'footer-1', 2 )->get();?>
            </div>

            <div class="ac_footer_primary_column">
               <?= Footer::content( 'footer-1', 3 )->get();?>
            </div>

            <div class="ac_footer_primary_column">
               <?= Footer::content( 'footer-1', 4 )->get();?>
            </div>
            
        </div>
    </div>

</section>