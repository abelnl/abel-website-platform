<?php

    use Abel\Helpers\Icon;
    use Abel\Helpers\Button;
    use Abel\Helpers\Image;
    /*

        TOPTASKS

    */


?>
<section data-s-type="<?= $view->type();?>" data-s-id="<?= $view->s_id();?>" data-s-amount="<?= $view->amount()?>" class="<?= $view->class('ac_content');?>" id="<?= $view->id();?>" <?= $view->sectionAnimation(); ?>>

    <?= $view->title(); ?>
    <div class="ac_toptasks_container <?= $view->containerMaxWidth(); ?> <?= $view->containerClass(); ?>">
        <div class="ac_toptasks_container_inner">

        <?php foreach ( $section->columns as $column): ?>
            <?php $button = $column->getField('button');?>
            <a href="<?= $button['link'] ?>" <?= (array_key_exists('target', $button) && ($button['target'] || $button['target'] == 'true') && $button['target'] != 'false')?'target="_blank"':'' ?> class="ac_toptasks_item" data-s-amount-item <?= $view->elementAnimation(); ?>>
                <div class="ac_toptasks_item_container">
                   
                    <div class="ac_toptasks_item_image-container" style="display:block;">
                        
                        <?php if( Image::set( $column->getField( 'image' ) ) ): ?>
                            <figure class="ac_toptasks_item_image <?= Image::alignment( ); ?>" data-interchange="[<?= Image::url( null, '600w') ?>, small], [<?= Image::url( null, '1000w') ?>, medium], [<?= Image::url( null, '1600w') ?>, large]" style="background-image: url(<?= Image::url( null, '1600w') ?>)"></figure>
                        <?php endif; ?>
                        
                    </div>
                    
                    <?php if( $column->getField('icon', '' ) != '' && $column->getField('icon') != 'none'):
                    ?>
                    <div class="ac_toptasks_item_icon-container">
                        <div class="ac_toptasks_item_icon">
                            <?= Icon::get( $column->getField('icon', '' ) );?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="ac_toptasks_item_content">
                        <?php 
                            $title = $column->getField('title');
                            if ($title && is_array($title) ) {
                                $title = $title['text'];
                            }else {
                                $title = '';
                            }
                        ?>
                        <div class="ac_toptasks_item_content_title"><?= $title; ?></div>
                        <div class="ac_toptasks_item_content_description"><?php $column->theField('description');?></div>

                        <?php 
                        if( $button && is_array( $button ) && count( $button ) > 0 ):
                        ?>
                        <div class="ac_toptasks_item_content_btn-container">

                            <div href="<?= $button['link'];?>" <?= (array_key_exists('target', $button) && ($button['target'] || $button['target'] == 'true') && $button['target'] != 'false' )?'target="_blank"':'' ?>  class="<?= Button::getClass( $button );?>">
                                <?= $button['text']; 
                                ?>

                                <?php if( $button['icon'] != '' && $button['icon'] != 'none' ): ?>
                                <span class="svg-container">
                                    <?= Icon::get( $button['icon'] ); ?>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <?php endif; ?>
                    </div>
                </div>
            </a>
        <?php endforeach;?>
            
        </div>
    </div>

    <?php $view->end();?>
</section>