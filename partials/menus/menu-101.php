<?php
    use Abel\Helpers\Icon;
    use Abel\Wrappers\Header;
    use Abel\Front\Settings;
    /*  

        MENU 100

        Menu 101 is a vertical menu

    
    */


?>


<nav data-m-type="menu-101" data-m-version="1">

    <div class="ac_menu-101_container">

        <div class="ac_menu-101_logo_mobile_container">
            <a href="<?= get_home_url(); ?>/" class="ac_menu-101_logo_mobile_link">
                <img src="<?= Header::logo();?>" alt="" class="ac_menu-101_logo_mobile_img">
            </a>
        </div>

        <div class="ac_menu-101_main">
            <div class="ac_menu-101_main_top">

                <div class="ac_menu-101_logo_container">
                    <a href="<?= get_home_url(); ?>/" class="ac_menu-101_logo_link">
                        <img src="<?= Header::logo();?>" alt="" class="ac_menu-101_logo_img">
                    </a>
                </div>

                <div class="ac_menu-101_main_content">

                    <?= Header::content( 'menu-101', 1 )->get();?>

                </div>

            </div>
            <div class="ac_menu-101_main_bottom">
                <div class="ac_menu-101_main_content v_align-end">
                    
                    <?= Header::content( 'menu-101', 2 )->get();?>
                    <?= Header::content( 'menu-101', 3 )->get();?>

                    <br>
                    <br>
                    

                    <!-- v_hide_text v_smaller v_smallest v_collapse-on-small a_justify_flex-start a_justify_center a_justify_flex-end  -->

                    <?= Header::content( 'menu-101', 4 )->get();?>

                </div>
            </div>
        </div>

        <div class="ac_menu-101_mobile_content">
            
            <?= Header::mobile( 'cta' );?>
            <?= Header::mobile( 'search' );?>

            <div class="ac_menu-101_mobile_content_divider"></div>

            <button class="ac_menu-101_mobile_btn-hamburger" data-toggle-mobile-menu>
                <span class="burger-icon"></span>
            </button>                    
        </div>
    
        <?php if( !is_null( Header::mobile( 'search' ) ) ):?>
        <div class="ac_menu-101_search_container">
                <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                <input type="search" class="ac_menu-101_search_input" data-menu-search placeholder="Zoeken..." name="s" title="Zoeken" />
            </form>
            <button class="ac_menu-101_search_close" data-toggle-menu-search>
                <?= get_svg_symbol('icon_ui_close'); ?>
            </button>
        </div>
        <?php endif;?>

    </div>

    <div class="ac_menu-101_overlay_container">

        <div class="ac_menu-101_overlay_scroll_container">
            <div class="ac_menu-101_overlay_scroll_container_inner">

                <div class="ac_menu-101_overlay_menu_container">
                    <?= Header::mobile( 'menu' );?>
                    <hr>
                    <?= Header::mobile( 'socials' );?>
                </div>

                
            </div>
        </div>

    </div>
    <div class="ac_menu-101_overlay_backdrop" data-toggle-mobile-menu></div>

</nav>