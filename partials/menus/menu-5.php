<?php
    use Abel\Helpers\Icon;
    use Abel\Wrappers\Header;
    use Abel\Front\Settings;

    /*  

        MENU 5
    
    */

    $sticky = 'main'; // none or main
?>

<nav data-m-type="menu-5">

<?php if($sticky == 'main'): ?>
    <div class="ac_menu-5_unsticky_container"></div>
    <div class="ac_menu-5_sticky_container" id="menu-fixed-start" data-sticky-container>
        <div class="ac_menu-5_sticky sticky" data-sticky data-margin-top="0" data-top-anchor="menu-fixed-start:top" data-btm-anchor="page-end:top" data-sticky-on="small"> 
<?php endif ?>

    <div class="ac_menu-5_main">
    
        <div class="ac_menu-5_main_container">
           
            <div class="ac_menu-5_logo_container">
                <a href="<?= get_home_url(); ?>/" class="ac_menu-5_logo_link">
                    <img src="<?= Header::logo();?>" alt="" class="ac_menu-5_logo_img">
                </a>
            </div>

    
            <div class="ac_menu-5_main_content">
                <?= Header::content( 'menu-5', 1 )->get();?>
            </div>
            
            
            <div class="ac_menu-5_main_content">
        
                <?= Header::content( 'menu-5', 2 )->get();?>
                <?= Header::content( 'menu-5', 3 )->get();?>

            </div>
        </div>


        <div class="ac_menu-5_mobile_content">
            
            <?php
            $telphone = $useAltLogos = Settings::get( 'telephone', '' );
            if ( $telphone != ''):
            ?>
            <a href="tel:<?= $telphone ?>" class="ac_menu-5_mobile_btn">
                <?= get_svg_symbol('icon_ui_call'); ?>         
            </a>
            <?php
            endif;
            ?>

            <button class="ac_menu-5_mobile_btn " data-toggle-menu-search>
                <?= get_svg_symbol('icon_ui_search'); ?>
            </button>

            <div class="ac_menu-5_mobile_content_divider"></div>

            <button class="ac_menu-5_mobile_btn-hamburger" data-toggle-mobile-menu>
                <span class="burger-icon"></span>
            </button>     
        </div>
    
        <div class="ac_menu-5_search_container">
            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                <input type="search" class="ac_menu-5_search_input" data-menu-search placeholder="Zoeken..." name="s" title="Zoeken" />
            </form>
            <button class="ac_menu-5_search_close" data-toggle-menu-search>
                <?= get_svg_symbol('icon_ui_close'); ?>
            </button>
        </div>
        
    </div>

<?php if($sticky == 'main'): ?>
        </div>
    </div>
<?php endif ?>


    <div class="ac_menu-5_overlay_container">

        <div class="ac_menu-5_overlay_scroll_container">
            <div class="ac_menu-5_overlay_scroll_container_inner">

                <div class="ac_menu-5_overlay_menu_container">
                    <?= Header::mobile( 'menu' );?>

                    <?= Header::mobile( 'socials' );?>
                </div>
                
            </div>
        </div>

    </div>
    <div class="ac_menu-5_overlay_backdrop" data-toggle-mobile-menu></div>


</nav>