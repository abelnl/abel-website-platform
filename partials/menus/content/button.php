<?php 
use Abel\Helpers\Icon;
use Abel\Helpers\Button;
?>

<?php if( $button && is_array($button) && count($button) > 0 && $button['link'] != ''):?>
<a href="<?= $button['link'];?>"  <?= (array_key_exists('target', $button) && ($button['target'] || $button['target'] == 'true'))?'target="_blank"':'' ?> class="<?= Button::getClass( $button );?>">
    <?= $button['text'] ?>
    <?php if( $button['icon'] != '' && $button['icon'] != 'none' ): ?>
    <span class="svg-container">
        <?= Icon::get( $button['icon'] ); ?>
    </span>
    <?php endif; ?>
</a> 
<?php endif; ?>   