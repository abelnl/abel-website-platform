<?php use Abel\Helpers\Icon;?>
<div class="ac_<?= $menu ?>_cta">
                                    
    <span class="ac_<?= $menu ?>_cta_text"><?= $text ?></span> 
    <?php
        if ( $link && $link != '' ):
    ?>
    <a href="<?= $link ?>" class="ac_<?= $menu ?>_cta_link"> 
        <?php 
            if( strtolower($icon) != 'none' )
                echo $icon; 
            echo $btnText;
            
        ?>
    </a>
    <?php
        endif;
    ?>

</div>