 <?php 

use Abel\Helpers\Icon;

if( (string)$form == 'true' ):?>

	<form role="search" method="get" class="ac_<?= $menu?>_search-form" action="<?php echo home_url( '/' ); ?>">
		<input type="search" class="ac_<?= $menu;?>_search_input" data-menu-search placeholder="<?= $placeholder;?>" name="s" title="<?= $placeholder;?>" />
	</form>

<?php else:?>

	<a href="#" data-toggle-menu-search class="ac_<?= $menu ?>_btn-search">
		<?= $btnText ?>
		<?php 
			if( strtolower($icon) != 'none' )
                echo $icon;
		?>
	</a>
	<div class="ac_<?= $menu ?>_search_container">
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			<input type="search" class="ac_<?= $menu ?>_search_input" data-menu-search placeholder="<?= $placeholder ?>" name="s" title="<?= $placeholder ?>" />
		</form>
		<button class="ac_<?= $menu ?>_search_close" data-toggle-menu-search>
			<?= Icon::get('icon_ui_close'); ?>
		</button>
	</div>

<?php endif;?>