<?php 
if($type != '') {
wp_nav_menu([ 
	'menu' => $menuId,
	'items_wrap' => 
		'<ul id="%1$s" class="%2$s dropdown ac_'.$menu.'_menu" data-dropdown-menu data-alignment="'.$align.'">%3$s</ul>',
	'depth' => 2
]);
} else {
wp_nav_menu([ 
	'menu' => $menuId,
	'items_wrap' => 
		'<ul id="%1$s" class="%2$s ac_menu-'.$menu.'_overlay_menu_primary vertical menu" data-accordion-menu data-multi-open="false">%3$s</ul>',
	'depth' => 2
]);
}


?>