<?php
    use Abel\Helpers\Icon;
    use Abel\Wrappers\Header;
    use Abel\Front\Settings;

    /*  

        MENU 2

    */

    $sticky = 'main-and-bottom'; // none, main, main-and-bottom or bar-bottom
?>

<nav data-m-type="menu-2">

    <div class="ac_menu-2_bar_top">
        <div class="ac_menu-2_bar_top_container">

            <div class="ac_menu-2_bar_top_content">

                <?php
                    /*
                        This container can hold any kind of content, like a secondary menu, search, or CTA
                    */
                ?>

                <?= Header::content( 'menu-2', 1 )->get();?>
            </div>

            <div class="ac_menu-2_bar_top_content">
                <?php
                    /*
                        This container can hold any kind of content, like a secondary menu, search, or CTA
                    */
                ?>
                <?= Header::content( 'menu-2', 2 )->get();?>

            </div>

        </div>
    </div>

<?php if($sticky == 'main-and-bottom'): ?>
    <div class="ac_menu-2_sticky_container" id="menu-fixed-start" data-sticky-container>
        <div class="ac_menu-2_sticky sticky" data-sticky data-margin-top="0" data-top-anchor="menu-fixed-start:top" data-btm-anchor="page-end:top" data-sticky-on="small"> 
<?php endif ?>


<?php if($sticky == 'main'): ?>
    <div class="ac_menu-2_sticky_container" id="menu-fixed-start" data-sticky-container>
        <div class="ac_menu-2_sticky sticky" data-sticky data-margin-top="0" data-top-anchor="menu-fixed-start:top" data-btm-anchor="page-end:top" data-sticky-on="small"> 
<?php endif ?>

    <div class="ac_menu-2_main">
    
        <div class="ac_menu-2_main_container">
           
            <div class="ac_menu-2_logo_container">
                <a href="<?= get_home_url(); ?>/" class="ac_menu-2_logo_link">
                    <img src="<?= Header::logo();?>" alt="" class="ac_menu-2_logo_img">
                </a>
            </div>

            <?php
                /*
                    This container is optional
                */
            ?>
            <div class="ac_menu-2_main_content">
               <?= Header::content( 'menu-2', 3 )->get();?>
            </div>

            
            <div class="ac_menu-2_main_content">
                <div class="ac_menu-2_main_content">
                    <?php
                        /*
                            This container can hold any kind of content, like a secondary menu, search, or CTA
                        */
                    ?>
                    
                    <?= Header::content( 'menu-2', 4 )->get();?>

                </div>
            </div>
        </div>


        <div class="ac_menu-2_mobile_content">
            <?php
            $telphone = $useAltLogos = Settings::get( 'telephone', '' );
            if ( $telphone != ''):
            ?>
            <a href="tel:<?= $telphone ?>" class="ac_menu-2_mobile_btn">
                <?= get_svg_symbol('icon_ui_call'); ?>         
            </a>
            <?php
            endif;
            ?>

            <button class="ac_menu-2_mobile_btn " data-toggle-menu-search>
                <?= get_svg_symbol('icon_ui_search'); ?>
            </button>

            <div class="ac_menu-2_mobile_content_divider"></div>

            <button class="ac_menu-2_mobile_btn-hamburger" data-toggle-mobile-menu>
                <span class="burger-icon"></span>
            </button>                    

        </div>
        
        <div class="ac_menu-2_search_container">
            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                <input type="search" class="ac_menu-2_search_input" data-menu-search placeholder="Zoeken..." name="s" title="Zoeken" />
            </form>
            <button class="ac_menu-2_search_close" data-toggle-menu-search>
                <?= get_svg_symbol('icon_ui_close'); ?>
            </button>
        </div>

    </div>

<?php if($sticky == 'main'): ?>
        </div>
    </div>
<?php endif ?>

<?php if($sticky == 'bar-bottom'): ?>
    <div class="ac_menu-2_sticky_container" id="menu-fixed-start" data-sticky-container>
        <div class="ac_menu-2_sticky sticky" data-sticky data-margin-top="0" data-top-anchor="menu-fixed-start:top" data-btm-anchor="page-end:top" data-sticky-on="small"> 
<?php endif ?>

            <div class="ac_menu-2_bar_bottom">
                <div class="ac_menu-2_bar_bottom_container">

                    <div class="ac_menu-2_bar_bottom_content">

                        <?php
                            /*
                                This container can hold any kind of content, like a secondary menu, search, or CTA
                            */
                        ?>

                        <?= Header::content( 'menu-2', 5 )->get();?>

                    </div>

                    <div class="ac_menu-2_bar_bottom_content">

                        <?php
                            /*
                                This container can hold any kind of content, like a secondary menu, search, or CTA
                            */
                        ?>

                        <?= Header::content( 'menu-2', 6 )->get();?>
                
                    </div>

                </div>           
            </div>

<?php if($sticky == 'bar-bottom'): ?>
        </div>
    </div>
<?php endif ?>

<?php if($sticky == 'main-and-bottom'): ?>
        </div>
    </div>
<?php endif ?>


    <div class="ac_menu-2_overlay_container">

        <div class="ac_menu-2_overlay_scroll_container">
            <div class="ac_menu-2_overlay_scroll_container_inner">

                <div class="ac_menu-2_overlay_menu_container">
                    
                    <?= Header::mobile( 'menu' );?>
    
                    <?= Header::mobile( 'socials' );?>
                    
                </div>
                
            </div>
        </div>

    </div>
    <div class="ac_menu-2_overlay_backdrop" data-toggle-mobile-menu></div>


</nav>