<?php

    use Abel\Helpers\Icon;
    use Abel\Wrappers\Header;
    use Abel\Front\Settings;
    /*  

        MENU 1

    */

    $sticky = true; 

?>


<nav data-m-type="menu-1">

<?php if($sticky): ?>
    <div class="ac_menu-1_unsticky_container"></div>
    <div class="ac_menu-1_sticky_container" id="menu-start" data-sticky-container>
        <div class="ac_menu-1_sticky sticky" data-sticky data-margin-top="0" data-top-anchor="menu-start:top" data-btm-anchor="page-end:top" data-sticky-on="small"> 
<?php endif ?>

            <div class="ac_menu-1_container">
                
                <div class="ac_menu-1_logo_container">
                    <a href="<?= get_home_url(); ?>/" class="ac_menu-1_logo_link">
                        <img src="<?= Header::logo();?>" alt="" class="ac_menu-1_logo_img">
                    </a>
                </div>

                <div class="ac_menu-1_content_container">
                    <div class="ac_menu-1_content_secondary">
                        <?= Header::content( 'menu-1', 1 )->get();?>
                        <?= Header::content( 'menu-1', 2 )->get();?>
                    </div>
                    <div class="ac_menu-1_content_primary">
                        <?= Header::content( 'menu-1', 3 )->get();?>                    
                    </div>
                </div>

                <div class="ac_menu-1_mobile_content">
                    <?php
                    $telphone = $useAltLogos = Settings::get( 'telephone', '' );
                    if ( $telphone != ''):
                    ?>
                    <a href="tel:<?= $telphone ?>" class="ac_menu-1_mobile_btn">
                        <?= get_svg_symbol('icon_ui_call'); ?>         
                    </a>
                    <?php
                    endif;
                    ?>

                    <button class="ac_menu-1_mobile_btn " data-toggle-menu-search>
                        <?= get_svg_symbol('icon_ui_search'); ?>
                    </button>

                    <div class="ac_menu-1_mobile_content_divider"></div>

                    <button class="ac_menu-1_mobile_btn-hamburger" data-toggle-mobile-menu>
                        <span class="burger-icon"></span>
                    </button>                
                </div>
    
                <div class="ac_menu-1_search_container">
                     <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                        <input type="search" class="ac_menu-1_search_input" data-menu-search placeholder="Zoeken..." name="s" title="Zoeken" />
                    </form>
                    <button class="ac_menu-1_search_close" data-toggle-menu-search>
                        <?= get_svg_symbol('icon_ui_close'); ?>
                    </button>
                </div>

            </div>

<?php if($sticky): ?>
        </div>
    </div>
<?php endif ?>

    <div class="ac_menu-1_overlay_container">

        <div class="ac_menu-1_overlay_scroll_container">
            <div class="ac_menu-1_overlay_scroll_container_inner">

                <div class="ac_menu-1_overlay_menu_container">
                    
                    <?= Header::mobile( 'menu' );?>

                    <?= Header::mobile( 'socials' );?>
                    
                </div>
                
            </div>
        </div>

    </div>
    <div class="ac_menu-1_overlay_backdrop" data-toggle-mobile-menu></div>


</nav>