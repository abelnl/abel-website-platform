<?php 
    
    use Abel\Helpers\Service;
    use Abel\Helpers\Icon;
    use Abel\Helpers\Image;
    
    Service::set( $item );
?>
<?php
if ( $view->showCollectionLinks() ):
?>
<a href="<?= Service::link();?>" class="ac_item" data-s-amount-item data-anime-elem>
<?php
else:
?>
<div class="ac_item" data-s-amount-item data-anime-elem>
<?php
endif;
?>
    <div class="ac_item_container" data-border-bottom>
        <?php 
            $imgId = Service::thumbId(); 
            if(!is_null($imgId) ):
        ?>
        <div class="ac_item_image-container">
            <figure class="ac_item_image <?= Image::alignment( $imgId ); ?>" data-interchange="[<?= Image::url( $imgId, '600w') ?>, small]" style="background-image: url(<?= Image::url( $imgId, '600w') ?>)"></figure>
        </div>
        <?php endif; ?>
        <div class="ac_item_content">
            <div class="ac_item_icon">
                <?= Icon::get('icon_ui_arrow-right'); ?>
            </div>
            <div class="ac_item_content_copy">
                <div class="ac_item_content_icon_container">
                    <div class="ac_item_content_icon">
                        <?= Service::icon()?>
                    </div>                            
                </div>
                <div class="ac_item_content_copy-above">
                    <?= Service::subtitle()?>
                </div>
                <div class="ac_item_content_title">
                    <?= Service::title()?>
                </div>
                <div class="ac_item_content_copy-below">
                    <?= Service::description()?>
                </div>
            </div>
        </div>
    </div>
<?php
if ( $view->showCollectionLinks() ):
?>
</a>
<?php
else:
?>
</div>
<?php
endif;
?>