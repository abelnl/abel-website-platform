<?php
    use Cuisine\Wrappers\Script;
	use \Cuisine\Utilities\Url;

    get_header();

?>
    <div class="main-container">
<?php

?>
        <section data-s-type="content" class="a_padding-top-100 a_padding-bottom-100 a_bg_brand_white">
        
            <div class="ac_content_container a_collapsed a_max-width-800" style="min-height: 40vh">
                <div class="a_bg_brand_inherit a_padding-top-inherit a_padding-bottom-inherit">
                    
                    <div class="ac_content_flex">
                        <div class="ac_content_full">

                            <h2>404: pagina niet gevonden</h2>

                            <p>Deze pagina bestaat niet (meer).</p>

                            <hr>
                            
                            
                        </div>
                    </div>
                    
                </div>
            </div>

        </section>
<?php
    
    get_footer();  

?>