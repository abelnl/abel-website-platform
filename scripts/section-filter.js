jQuery(document).ready(function( $ ){

	if( $('.filter-items').length > 0 ){

		$( document ).on( 'click tap', '.filter', function( e ){

			e.preventDefault();

			if( $( this ).hasClass( 'active' ) === false ){

				$( '.filter' ).removeClass( 'active' );
				$( this ).addClass( 'active' );

				filterCollectionItems();

			}
		});

		$( '#submit-search' ).on( 'click tap', function( e ){

			e.preventDefault();
			filterCollectionItems();

		});

		$( '#search-query' ).on( 'keyup', function( e ){

			if( e.which == 13 ){
				e.preventDefault();
				filterCollectionItems();
			}

		});

	}


});


function filterCollectionItems(){

	var _collectionId = $( '.filter-items').data('collection-id');
	_collection = $( '#'+ _collectionId );

	var _section = $( '.filter-items' ).data('section-id');
	var _post = $( '.filter-items' ).data( 'post-id' );
	var _tax = $( '.filter-items').data('tax');
	var _message = $('.filter-items').data('message');


	var _activeFilter = $( '.filter.active' );

	_collection.addClass( 'loading' );

	//remove autoload functionality
	if( _activeFilter.data( 'filter') === 'all' ){
		_collection.removeClass( 'hold-autoload' );
	}else{
		_collection.addClass( 'hold-autoload' );
	}


	var data = {
		action: 'filter',
		page: 1,
		section: _section,
		post_id: _post,
		message: '',
		wrap: true
	}


	if( _activeFilter.data( 'filter' ) != 'all' ){
		data['filter_on'] = _tax;
		data['filter_val'] = _activeFilter.data( 'filter' );
	}

	if( $( '.search #search-query' ).val() != undefined && $( '.search #search-query' ).val() != '' )
		data['search'] = $( '.search #search-query' ).val();


	$.post( Cuisine.ajax, data, function( response ){

		
		_collection.removeClass( 'loading' );

		if( response === 'message' ){

			var _html = '<div class="ac_content_container"><div class="ac_content_flex">';
			_html += '<div class="ac_content_1-1 a_text-align_center">';
			_html += _message;
			_html += '</div></div></div>';
			
			_collection.html( _html );

		}else{
			_collection.html( response );
		}

	});
}

