/* PRODUCTS FEED
--------------------------------------------------------------------------------------------*/

(function () {

    $('section[data-productfeed]').each( function(){

        var _id = $(this).prop('id');
        _id = _id.split('-').join('_');
        var productFeed = window['feed_'+_id];

        if( typeof( productFeed ) !== 'undefined' && productFeed != '' ){
            productFeed = JSON.parse( productFeed );
            var productFeedAmount = productFeed.length;
            var results = {};
            var nLoaded = 0;

            function buildUI() {
                var content = '';
                productFeed.forEach(function (productFeedLink) {

                    var result = results[productFeedLink];
                    var description = result.description;
                    if (description.length > 80) description = description.substring(0, 80) + '...';

                    var sale = '';
                    if (result.sale == true) {
                        sale += '<div class="ac_item_content_label">Actie</div>';
                    }

                    var price = '';
                    if (result.price == result.old_price) {
                        price += '<div class="ac_item_content_price"><div class="ac_item_content_price_current">' + result.price + '</div></div>';
                    } else {
                        price += '<div class="ac_item_content_price">' +
                            '<div class="ac_item_content_price_before">van ' + result.old_price + ' </div>' +
                            '<div class="ac_item_content_price_current"> | voor ' + result.price + '</div>' +
                            '</div>';
                    }

                    content += '<a href="' + result.url + '" class="ac_item" target="_blank" data-s-amount-item>' +
                        '<div class="ac_item_container" data-border-bottom>' +
                        sale +
                        '<div class="ac_item_image-container">' +
                        '<figure class="ac_item_image a_contain" style="background-image: url(' + result.image + ')"></figure>' +
                        '</div>' +
                        '<div class="ac_item_content">' +
                        '<div class="ac_item_icon">' +
                        '<svg role="img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + Cuisine.baseUrl + '/wp-content/themes/abel/public/assets/symbols.svg#icon_ui_arrow-right"></use></svg>' +
                        '</div>' +
                        '<div class="ac_item_content_copy">' +
                        '<div class="ac_item_content_copy-above">' +
                        price +
                        '</div>' +
                        '<div class="ac_item_content_title">' +
                        result.title +
                        '</div>' +
                        '<div class="ac_item_content_copy-below">' +
                        description +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</a>';
                });

                $('#ac_products_feed_'+_id ).append(content);
            }
    
            $.each(productFeed, function (i, value) {
                console.log( productFeed[i]);
                $.ajax({
                    url: productFeed[i] + '?json=true',

                    success: function (result) {

                        results[productFeed[i]] = result;
                        nLoaded++;

                        if (nLoaded === productFeedAmount) {
                            buildUI();
                        }
                    }
                });
            });
        }
    });
})();