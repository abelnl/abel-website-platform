jQuery(function($){
    
    $('.ac_image-gallery_full-screen_container').slick();

    // close overlay on click outside image area
    $('.image-gallery_full-screen_item_image_container').on('click', function(e) {
        // console.log($(e.target));
        if ($(e.target).hasClass('image-gallery_full-screen_item_image_container')) {
            $('.ac_image-gallery_full-screen').removeClass('s_active');
            $('.ac_image-gallery_full-screen_background' ).removeClass('s_active');
            $('.ac_image-gallery_full-screen_close').removeClass('s_active')
        }   
        e.preventDefault();
    });

    $('.ac_image-gallery_full-screen_close').on('click tap', function() {
        $('.ac_image-gallery_full-screen').removeClass( 's_active' );
        $('.ac_image-gallery_full-screen_background' ).removeClass( 's_active' );
        $('.ac_image-gallery_full-screen_close').removeClass( 's_active' );
    });

    $('a.ac_image-gallery_item, a.ac_image-gallery_grid_item').on('click tap', function(e) {
        e.preventDefault();

        //if( $( this ).hasClass( 'ac_image-gallery_grid_item' ) ){
            var openOnSlide = $(this).data('s-amount-item');
       // }else{
        //    openOnSlide = parseInt( $(this).index() - 1 );
        //}
        var overlay = $( this ).parents("[data-overlay]:first").data('overlay');
        var _id = '#'+overlay;
        var _bgId = '#bg-'+overlay;
        var _closeId = '#close-'+overlay;

        $(_id+ ' .ac_image-gallery_full-screen_container').slick('slickGoTo',openOnSlide, true);
        $(_id+ ' .slick-active').trigger( 'click' );
        $(_id).addClass( 's_active' );
        $(_bgId ).addClass('s_active'); 
        $(_closeId).addClass('s_active');
    });

});