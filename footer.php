<?php
	use Abel\Front\Settings;
	use Abel\Wrappers\Footer;
	use Cuisine\Wrappers\Script;

	//render the chosen footer section:
	Footer::section();

	//render the cookie notice:
	$showCookieNotice = Settings::get( 'cookie_show', false );

	if ( $showCookieNotice && $showCookieNotice != 'false'  )
		get_template_part( 'partials/footers/cookienotice' ); 

	//render the footer:
	get_template_part( 'partials/footers/footer', Settings::get( 'footertype', '1' ) );    
	
	?>
	<!--  / main container -->
	</div>
	<div id="page-end"></div>

	<?php
	//allow plugin hooks
	wp_footer(); 
	
	//render the hotjar code:
	get_template_part( 'partials/footers/hotjar' );  
		
    Script::setVars( false ); 
	
	?>
	
	</body>
</html> 