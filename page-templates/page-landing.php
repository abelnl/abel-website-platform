<?php
    use Cuisine\Wrappers\Script;
    use \Cuisine\Utilities\Url;
    use Abel\Wrappers\Header;
	use Abel\Front\Settings;
	use Abel\Wrappers\Footer;

	/*
	    Template Name: Landing
	*/


    get_template_part( 'partials/header', 'meta' );
?>
    
    <div class="ac_menu-landing">
        <div class="ac_menu-landing_logo_container">
            <img src="<?= Header::logo();?>" alt="" class="ac_menu-landing_logo_img">
        </div>
    </div>

<?php
    echo '<div class="main-container">';


    the_sections();  
    
    
    //render the cookie notice:
    $showCookieNotice = Settings::get( 'cookie_show', false );

    if ( $showCookieNotice && $showCookieNotice != 'false'  )
        get_template_part( 'partials/footers/cookienotice' ); 

    
    ?>
    <!--  / main container -->
    </div>
    <div id="page-end"></div>

    <?php
    //allow plugin hooks
    wp_footer(); 

    $ga = Settings::get( 'ga_code' );
    if ($ga && $ga != '')
        Script::analytics( $ga );

    //render the hotjar code:
    get_template_part( 'partials/footers/hotjar' );  
        
    Script::setVars( false ); 
    
    ?>
    
    </body>
</html> 
    