<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	
	<div class="input-group">
		<input type="search" class="search-field" placeholder="Zoeken.." value="<?php echo get_search_query() ?>" name="s" title="zoeken" />
		<div class="input-group-button">
			<input type="submit" class="button" value="zoeken" />
		</div>
	</div>

</form>